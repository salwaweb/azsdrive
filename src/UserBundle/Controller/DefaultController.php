<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\User;
use GestionPoleBundle\Form\CommandType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        if(true== $this->get('security.authorization_checker')->isGranted('ROLE_RECEPTION'))
        {
            $em = $this->getDoctrine()->getManager();

            $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();

            return $this->render('GestionPoleBundle:pole-commande:index.html.twig', array(
                'commandes' => $commandes,
            ));

        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN'))

        {
            return $this->render('GestionPoleBundle:admin:admin.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))

        {
            return $this->render('GestionPoleBundle:pole-production:index.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_QUALITE'))

        {
            return $this->render('GestionPoleBundle:pole-qualite:index.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_LOGISTIQUE'))

        {
            return $this->render('GestionPoleBundle:pole-logistique:index.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_TECHNIQUE'))

        {
            return $this->render('GestionPoleBundle:pole-technique:index.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN'))

        {
            return $this->render('GestionPoleBundle:admin:admin.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_USER'))

        {
            $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
            //dd($utilisateur);
            return $this->render('GestionPoleBundle:client:client.html.twig', array(
                'utilisateur' => $utilisateur,));
        }
        if(true== $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT'))
        {
            $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
            //dd($utilisateur);
            return $this->render('UserBundle:Default:user.html.twig', array(
                'utilisateur' => $utilisateur,));
        }
        return $this->render('UserBundle:Default:index.html.twig');
    }

}
