<?php

namespace GestionPoleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('entreprise')
            ->add('num_entreprise')
            ->add('sex', ChoiceType::class, array(
                'choices' => array(
                    'Civilité' => '',
                    'Mr' => 'Mr',
                    'Mme' => 'Mme',
                    'Mlle' => 'Mlle',

                ),
                'preferred_choices' => array('muppets', 'arr'),
            ))

            ->add('prenom')
            ->add('email', EmailType::class, array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))

            ->add('region', ChoiceType::class, array(
                'choices' => array(
                    'Choisissez la région' => '',
                    'Guyane' => 'Guyane',
                    'La Réunion' => 'La Réunion',
                    'Corse' => 'Corse',
                    'Martinique' => 'Martinique',
                    'Franche-Comté' => 'Franche-Comté',
                    'Île-de-France' => 'Île-de-France',
                    'Centre-Val de Loire' => 'Centre-Val de Loire',
                    'Bretagne' => 'Bretagne',
                    'Pays de la Loire' => 'Pays de la Loire',
                    'Auvergne' => 'Auvergne',
                    'Nord-Pas-de-Calais' => 'Nord-Pas-de-Calais',


                ),
                'preferred_choices' => array('muppets', 'arr'),
            ))



            ->add('adresse')
            ->add('codpostal')
            ->add('ville')
            ->add('tel')


            ->add('cgu', CheckboxType::class, array(
                'mapped' => false,
                'constraints' => new IsTrue(),
            ))



            ->add('enabled')


        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionPoleBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionpolebundle_user';
    }


}
