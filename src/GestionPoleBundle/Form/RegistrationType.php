<?php
// src/AppBundle/Form/RegistrationType.php

namespace GestionPoleBundle\Form;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('entreprise')
            ->add('num_entreprise')
            ->add('sex', ChoiceType::class, array(
                'choices' => array(
                    'Civilité' => '',
                    'Mr' => 'Mr',
                    'Mme' => 'Mme',
                    'Mlle' => 'Mlle',

                ),
                'preferred_choices' => array('muppets', 'arr'),
            ))

            ->add('prenom')

            ->add('region', ChoiceType::class, array(
                'choices' => array(
                    'Choisissez la région' => '',
                    'Guyane' => 'Guyane',
                    'La Réunion' => 'La Réunion',
                    'Corse' => 'Corse',
                    'Martinique' => 'Martinique',
                    'Franche-Comté' => 'Franche-Comté',
                    'Île-de-France' => 'Île-de-France',
                    'Centre-Val de Loire' => 'Centre-Val de Loire',
                    'Bretagne' => 'Bretagne',
                    'Pays de la Loire' => 'Pays de la Loire',
                    'Auvergne' => 'Auvergne',
                    'Nord-Pas-de-Calais' => 'Nord-Pas-de-Calais',


                ),
                'preferred_choices' => array('muppets', 'arr'),
            ))



            ->add('adresse')
            ->add('codpostal')
            ->add('ville')
            ->add('tel')

            ->add('cgu', CheckboxType::class, array(
                'mapped' => false,
                'constraints' => new IsTrue(),
            ))


        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}