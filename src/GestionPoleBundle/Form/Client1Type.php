<?php
// src/AppBundle/Form/RegistrationType.php

namespace GestionPoleBundle\Form;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Form\FormBuilderInterface;

class Client1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'required'  => true,
                // render check-boxes
                'choices' => [

                    'Activer ce Client' => 'ROLE_CLIENT',


                    // ...
                ], ])


        ;
    }



    public function getName()
    {
        return 'app_user_registration';
    }
}