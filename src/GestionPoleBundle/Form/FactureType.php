<?php

namespace GestionPoleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FactureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
		->add('datech', DateType::class, array(
                'widget' => 'single_text',
            ))
		->add('regl')
		->add('notva')
		->add('prix')
		->add('modep', ChoiceType::class, [
    'choices'  => [
        'Chèque' => 'Chèque',
        'virement bancaire' => 'Virement bancaire.',
        'à la livraison' => 'à la livraison',
    ],
])
		->add('transport');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionPoleBundle\Entity\Facture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionpolebundle_facture';
    }


}
