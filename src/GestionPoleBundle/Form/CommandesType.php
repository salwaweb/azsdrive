<?php

namespace GestionPoleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CommandesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numcompte')->add('numpiece')->add('ac')
            ->add('dateDeco', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('datedep', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('dateliv', DateType::class, array(
                'widget' => 'single_text',
            ))

            ->add('imperatif')

            ->add('refarticle')
            ->add('designation')
            ->add('refcdclient')
            ->add('qtecdee')
            ->add('dec1')
            ->add('dec2')
            ->add('autredec')
            ->add('couleur')
            ->add('position')
            ->add('cdebat')
            ->add('condpal')
            ->add('cp')
            ->add('villedep')
            ->add('instruction1')
            ->add('expedition')
            ->add('info1c')
            ->add('info2c')
            ->add('condinfo')
            ->add('upinfo')
            ->add('controleinfo')
            ->add('devers1info')
            ->add('devers2info')
            ->add('liv_azs')
            ->add('prix_dec')
            ->add('ft')
            ->add('bat',MediaType::class, ['required' => false])
            ->add('eplt',MediaType::class, ['required' => false])
            ->add('cxsl',MediaType::class, ['required' => false])
            ->add('cpdf',MediaType::class, ['required' => false])
            ->add('conditionnement', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'required'  => true,
                // render check-boxes
                'choices' => [

                    'Conditionnement normal ' => '1',
                    'Conditionnement spécifique' => '2',
                    // ...
                ], ])
            ->add('description')



       /* ->add('traitement', ChoiceType::class, [
        'multiple' => false,
        'expanded' => true,
        'required'  => true,
        // render check-boxes
        'choices' => [

            'type1' => 'type 1',
            'type2' => 'type 2',
            'type 3' => 'type 3',


            // ...
        ], ])*/
       ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionPoleBundle\Entity\Commandes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionpolebundle_commandes';
    }


}
