<?php

namespace GestionPoleBundle\Form;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class Commande3Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('etatqlt', CheckboxType::class, array(
            'label'    => 'Valider cette commande?',
            'required' => false,
        ))
            ->add('refuse', CheckboxType::class, array(
                'label'    => 'Refuser cette commande ?',
                'required' => false,
            ))

            ->add('Commentaire', TextareaType::class, array(
                'attr' => array('class' => 'form-control',
                    'placeholder' => 'Si refuser dit pourquoi? ',
                    'empty_value' => true,
                    'required' => false)
            ))
        ;


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GestionPoleBundle\Entity\Commandes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gestionpolebundle_commandes';
    }


}
