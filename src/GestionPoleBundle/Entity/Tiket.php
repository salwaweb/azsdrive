<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tiket
 *
 * @ORM\Table(name="tiket")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\TiketRepository")
 */
class Tiket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Sav", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */

    private $support;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */

    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set message
     *
     * @param string $message
     *
     * @return Tiket
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set support
     *
     * @param \GestionPoleBundle\Entity\Sav $support
     *
     * @return Tiket
     */
    public function setSupport(\GestionPoleBundle\Entity\Sav $support)
    {
        $this->support = $support;

        return $this;
    }

    /**
     * Get support
     *
     * @return \GestionPoleBundle\Entity\Sav
     */
    public function getSupport()
    {
        return $this->support;
    }



    /**
     * Set sender
     *
     * @param \GestionPoleBundle\Entity\User $sender
     *
     * @return Tiket
     */
    public function setSender(\GestionPoleBundle\Entity\User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }
}
