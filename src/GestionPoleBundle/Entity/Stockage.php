<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stockage
 *
 * @ORM\Table(name="stockage")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\StockageRepository")
 */
class Stockage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;
    /**
     * @var bool
     *
     * @ORM\Column(name="statut", type="boolean", nullable=true)
     */
    private $statut;
    /**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Commandes", mappedBy="refarticle", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $commandes;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referance
     *
     * @param string $referance
     *
     * @return Stockage
     */
    public function setReferance($referance)
    {
        $this->referance = $referance;

        return $this;
    }

   

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Stockage
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Stockage
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Stockage
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Stockage
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commande = new \Doctrine\Common\Collections\ArrayCollection();
    }

 



    /**
     * Add commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Stockage
     */
    public function addCommande(\GestionPoleBundle\Entity\Commandes $commande)
    {
        $this->commandes[] = $commande;

        return $this;
    }

    /**
     * Remove commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     */
    public function removeCommande(\GestionPoleBundle\Entity\Commandes $commande)
    {
        $this->commandes->removeElement($commande);
    }

    /**
     * Get commandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandes()
    {
        return $this->commandes;
    }
    public function __toString(){
        // to show the name of the Category in the select
        return $this->reference;
        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * Set utilisateur
     *
     * @param \GestionPoleBundle\Entity\User $utilisateur
     *
     * @return Stockage
     */
    public function setUtilisateur(\GestionPoleBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

  

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Stockage
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statut
     *
     * @param boolean $statut
     *
     * @return Stockage
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return boolean
     */
    public function getStatut()
    {
        return $this->statut;
    }
}
