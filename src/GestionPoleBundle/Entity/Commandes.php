<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commandes
 *
 * @ORM\Table(name="commandes")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\CommandesRepository")
 */
class Commandes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;
    /**
     * @var string
     *
     * @ORM\Column(name="numcomd", type="string", length=255)
     */
    private $numcomd;

    /**
     * @var string
     *
     * @ORM\Column(name="numcompte", type="string", length=255, unique=true)
     */
    private $numcompte;

    /**
     * @var string
     *
     * @ORM\Column(name="numpiece", type="string", length=255)
     */
    private $numpiece;

    /**
     * @var string
     *
     * @ORM\Column(name="ac", type="string", length=255)
     */
    private $ac;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deco", type="date")
     */
    private $dateDeco;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_liv", type="date")
     */
    private $dateliv;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedep", type="date")
     */
    private $datedep;

    /**
     * @var string
     *
     * @ORM\Column(name="imperatif", type="string", length=255)
     */
    private $imperatif;


    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Stockage", inversedBy="commande")
     * @ORM\JoinColumn(nullable=true)
     */

    private $refarticle;


    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="refcdclient", type="string", length=255, nullable=true)
     */
    private $refcdclient;

    /**
     * @var int
     *
     * @ORM\Column(name="qtecdee", type="integer")
     */
    private $qtecdee;

    /**
     * @var string
     *
     * @ORM\Column(name="dec1", type="string", length=255)
     */
    private $dec1;

    /**
     * @var string
     *
     * @ORM\Column(name="dec2", type="string", length=255, nullable=true)
     */
    private $dec2;

    /**
     * @var string
     *
     * @ORM\Column(name="autredec", type="string", length=255, nullable=true)
     */
    private $autredec;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=255)
     */
    private $couleur;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="cdebat", type="string", length=255)
     */
    private $cdebat;

    /**
     * @var string
     *
     * @ORM\Column(name="condpal", type="string", length=255, nullable=true)
     */
    private $condpal;

    /**
     * @var int
     *
     * @ORM\Column(name="cp", type="integer")
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="villedep", type="string", length=255)
     */
    private $villedep;

    /**
     * @var string
     *
     * @ORM\Column(name="instruction1", type="string", length=255, nullable=true)
     */
    private $instruction1;


    /**
     * @var string
     *
     * @ORM\Column(name="expedition", type="string", length=255, nullable=true)
     */
    private $expedition;

    /**
     * @var string
     *
     * @ORM\Column(name="info1c", type="string", length=255, nullable=true)
     */
    private $info1c;

    /**
     * @var string
     *
     * @ORM\Column(name="info2c", type="string", length=255, nullable=true)
     */
    private $info2c;


    /**
     * @var string
     *
     * @ORM\Column(name="condinfo", type="string", length=255, nullable=true)
     */
    private $condinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="upinfo", type="string", length=255, nullable=true)
     */
    private $upinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="controleinfo", type="string", length=255, nullable=true)
     */
    private $controleinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="devers1info", type="string", length=255, nullable=true)
     */
    private $devers1info;

    /**
     * @var string
     *
     * @ORM\Column(name="devers2info", type="string", length=255, nullable=true)
     */
    private $devers2info;
    /**
     * @var bool
     *
     * @ORM\Column(name="liv_azs", type="boolean", nullable=true)
     */
    private $liv_azs;
    /**
     * @var int
     *
     * @ORM\Column(name="prix_dec", type="integer",nullable=true)
     */
    private $prix_dec;
    /**
     * @var int
     *
     * @ORM\Column(name="ft", type="integer",nullable=true)
     */
    private $ft;

    /**
     * @var bool
     *
     * @ORM\Column(name="etatcomd", type="boolean", nullable=true)
     */
    private $etatcomd;
    /**
     * @var bool
     *
     * @ORM\Column(name="marchandise", type="boolean", nullable=true)
     */
    private $marchandise;
    /**
     * @var bool
     *
     * @ORM\Column(name="ecran", type="boolean", nullable=true)
     */
    private $ecran;
    /**
     * @var string
     *
     * @ORM\Column(name="conditionnemen", type="string", length=255, nullable=true)
     */
    private $conditionnement;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="traitement", type="string", length=255, nullable=true)
     */
    private $traitement;
    /**
     * @var bool
     *
     * @ORM\Column(name="etatprod", type="boolean", nullable=true)
     */
    private $etatprod;
    /**
     * @var bool
     *
     * @ORM\Column(name="prodfin", type="boolean", nullable=true)
     */
    private $prodfin;
    /**
     * @var bool
     *
     * @ORM\Column(name="etatqlt", type="boolean", nullable=true)
     */
    private $etatqlt;
    /**
     * @var bool
     *
     * @ORM\Column(name="etattech", type="boolean", nullable=true)
     */
    private $etattech;
    /**
     * @var bool
     *
     * @ORM\Column(name="etatlog", type="boolean", options={"default":"0"})
     */
    private $etatlog;
    /**
     * @var bool
     *
     * @ORM\Column(name="refuse", type="boolean", nullable=true)
     */
    private $refuse;
    /**
     * @var bool
     *
     * @ORM\Column(name="retouche", type="boolean", nullable=true)
     */
    private $retouche;
    /**
     * @var string
     *
     * @ORM\Column(name="Commentaire", type="text", nullable=true)
     */
    private $Commentaire;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_prd", type="date", nullable=true)
     */
    private $dateprd;

    /**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Etiquette", mappedBy="commande", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $etiquette;
    /**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Palette", mappedBy="commande", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $palette;
    /**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Retiquette", mappedBy="commande", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $retiquette;

    /**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Fabrication", mappedBy="commande", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $fabrication;
    /**
     * @var bool
     *
     * @ORM\Column(name="misejour", type="boolean", options={"default":"0"})
     */
    private $misejour;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="annee", type="date")
     */
    private $annee;
  /**
     * @var bool
     *
     * @ORM\Column(name="bl", type="boolean", nullable=true)
     */
    private $bl;
	 /**
     * @var bool
     *
     * @ORM\Column(name="certifie", type="boolean", nullable=true)
     */
    private $certifie;
	 /**
     * @var bool
     *
     * @ORM\Column(name="facture", type="boolean", nullable=true)
     */
    private $facture;
	/**
     * @ORM\OneToMany(targetEntity="GestionPoleBundle\Entity\Facture", mappedBy="commande", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */

    private $fact;
 /**
     * @var bool
     *
     * @ORM\Column(name="etiq", type="boolean", nullable=true)
     */
    private $etiq;

    /**
     * @var bool
     *
     * @ORM\Column(name="plt", type="boolean", nullable=true)
     */
    private $plt;

    /**
     * @var bool
     *
     * @ORM\Column(name="retq", type="boolean", nullable=true)
     */
    private $retq;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $bat;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $eplt;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $cxsl;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $cpdf;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numcompte
     *
     * @param string $numcompte
     *
     * @return Commandes
     */
    public function setNumcompte($numcompte)
    {
        $this->numcompte = $numcompte;

        return $this;
    }

    /**
     * Get numcompte
     *
     * @return string
     */
    public function getNumcompte()
    {
        return $this->numcompte;
    }

    /**
     * Set numpiece
     *
     * @param string $numpiece
     *
     * @return Commandes
     */
    public function setNumpiece($numpiece)
    {
        $this->numpiece = $numpiece;

        return $this;
    }

    /**
     * Get numpiece
     *
     * @return string
     */
    public function getNumpiece()
    {
        return $this->numpiece;
    }

    /**
     * Set ac
     *
     * @param string $ac
     *
     * @return Commandes
     */
    public function setAc($ac)
    {
        $this->ac = $ac;

        return $this;
    }

    /**
     * Get ac
     *
     * @return string
     */
    public function getAc()
    {
        return $this->ac;
    }

    /**
     * Set dateDeco
     *
     * @param \DateTime $dateDeco
     *
     * @return Commandes
     */
    public function setDateDeco($dateDeco)
    {
        $this->dateDeco = $dateDeco;

        return $this;
    }

    /**
     * Get dateDeco
     *
     * @return \DateTime
     */
    public function getDateDeco()
    {
        return $this->dateDeco;
    }

    /**
     * Set datedep
     *
     * @param \DateTime $datedep
     *
     * @return Commandes
     */
    public function setDatedep($datedep)
    {
        $this->datedep = $datedep;

        return $this;
    }

    /**
     * Get datedep
     *
     * @return \DateTime
     */
    public function getDatedep()
    {
        return $this->datedep;
    }

    /**
     * Set imperatif
     *
     * @param string $imperatif
     *
     * @return Commandes
     */
    public function setImperatif($imperatif)
    {
        $this->imperatif = $imperatif;

        return $this;
    }

    /**
     * Get imperatif
     *
     * @return string
     */
    public function getImperatif()
    {
        return $this->imperatif;
    }



    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Commandes
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set refcdclient
     *
     * @param string $refcdclient
     *
     * @return Commandes
     */
    public function setRefcdclient($refcdclient)
    {
        $this->refcdclient = $refcdclient;

        return $this;
    }

    /**
     * Get refcdclient
     *
     * @return string
     */
    public function getRefcdclient()
    {
        return $this->refcdclient;
    }

    /**
     * Set qtecdee
     *
     * @param integer $qtecdee
     *
     * @return Commandes
     */
    public function setQtecdee($qtecdee)
    {
        $this->qtecdee = $qtecdee;

        return $this;
    }

    /**
     * Get qtecdee
     *
     * @return int
     */
    public function getQtecdee()
    {
        return $this->qtecdee;
    }

    /**
     * Set dec1
     *
     * @param string $dec1
     *
     * @return Commandes
     */
    public function setDec1($dec1)
    {
        $this->dec1 = $dec1;

        return $this;
    }

    /**
     * Get dec1
     *
     * @return string
     */
    public function getDec1()
    {
        return $this->dec1;
    }

    /**
     * Set dec2
     *
     * @param string $dec2
     *
     * @return Commandes
     */
    public function setDec2($dec2)
    {
        $this->dec2 = $dec2;

        return $this;
    }

    /**
     * Get dec2
     *
     * @return string
     */
    public function getDec2()
    {
        return $this->dec2;
    }

    /**
     * Set autredec
     *
     * @param string $autredec
     *
     * @return Commandes
     */
    public function setAutredec($autredec)
    {
        $this->autredec = $autredec;

        return $this;
    }

    /**
     * Get autredec
     *
     * @return string
     */
    public function getAutredec()
    {
        return $this->autredec;
    }

    /**
     * Set couleur
     *
     * @param string $couleur
     *
     * @return Commandes
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Commandes
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set cdebat
     *
     * @param string $cdebat
     *
     * @return Commandes
     */
    public function setCdebat($cdebat)
    {
        $this->cdebat = $cdebat;

        return $this;
    }

    /**
     * Get cdebat
     *
     * @return string
     */
    public function getCdebat()
    {
        return $this->cdebat;
    }

    /**
     * Set condpal
     *
     * @param string $condpal
     *
     * @return Commandes
     */
    public function setCondpal($condpal)
    {
        $this->condpal = $condpal;

        return $this;
    }

    /**
     * Get condpal
     *
     * @return string
     */
    public function getCondpal()
    {
        return $this->condpal;
    }

    /**
     * Set cp
     *
     * @param integer $cp
     *
     * @return Commandes
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return int
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set villedep
     *
     * @param string $villedep
     *
     * @return Commandes
     */
    public function setVilledep($villedep)
    {
        $this->villedep = $villedep;

        return $this;
    }

    /**
     * Get villedep
     *
     * @return string
     */
    public function getVilledep()
    {
        return $this->villedep;
    }

    /**
     * Set instruction1
     *
     * @param string $instruction1
     *
     * @return Commandes
     */
    public function setInstruction1($instruction1)
    {
        $this->instruction1 = $instruction1;

        return $this;
    }

    /**
     * Get instruction1
     *
     * @return string
     */
    public function getInstruction1()
    {
        return $this->instruction1;
    }

    /**
     * Set expedition
     *
     * @param string $expedition
     *
     * @return Commandes
     */
    public function setExpedition($expedition)
    {
        $this->expedition = $expedition;

        return $this;
    }

    /**
     * Get expedition
     *
     * @return string
     */
    public function getExpedition()
    {
        return $this->expedition;
    }

    /**
     * Set info1c
     *
     * @param string $info1c
     *
     * @return Commandes
     */
    public function setInfo1c($info1c)
    {
        $this->info1c = $info1c;

        return $this;
    }

    /**
     * Get info1c
     *
     * @return string
     */
    public function getInfo1c()
    {
        return $this->info1c;
    }

    /**
     * Set info2c
     *
     * @param string $info2c
     *
     * @return Commandes
     */
    public function setInfo2c($info2c)
    {
        $this->info2c = $info2c;

        return $this;
    }

    /**
     * Get info2c
     *
     * @return string
     */
    public function getInfo2c()
    {
        return $this->info2c;
    }

    /**
     * Set condinfo
     *
     * @param string $condinfo
     *
     * @return Commandes
     */
    public function setCondinfo($condinfo)
    {
        $this->condinfo = $condinfo;

        return $this;
    }

    /**
     * Get condinfo
     *
     * @return string
     */
    public function getCondinfo()
    {
        return $this->condinfo;
    }

    /**
     * Set upinfo
     *
     * @param string $upinfo
     *
     * @return Commandes
     */
    public function setUpinfo($upinfo)
    {
        $this->upinfo = $upinfo;

        return $this;
    }

    /**
     * Get upinfo
     *
     * @return string
     */
    public function getUpinfo()
    {
        return $this->upinfo;
    }

    /**
     * Set controleinfo
     *
     * @param string $controleinfo
     *
     * @return Commandes
     */
    public function setControleinfo($controleinfo)
    {
        $this->controleinfo = $controleinfo;

        return $this;
    }

    /**
     * Get controleinfo
     *
     * @return string
     */
    public function getControleinfo()
    {
        return $this->controleinfo;
    }

    /**
     * Set devers1info
     *
     * @param string $devers1info
     *
     * @return Commandes
     */
    public function setDevers1info($devers1info)
    {
        $this->devers1info = $devers1info;

        return $this;
    }

    /**
     * Get devers1info
     *
     * @return string
     */
    public function getDevers1info()
    {
        return $this->devers1info;
    }

    /**
     * Set devers2info
     *
     * @param string $devers2info
     *
     * @return Commandes
     */
    public function setDevers2info($devers2info)
    {
        $this->devers2info = $devers2info;

        return $this;
    }

    /**
     * Get devers2info
     *
     * @return string
     */
    public function getDevers2info()
    {
        return $this->devers2info;
    }

  

    /**
     * Set utilisateur
     *
     * @param \GestionPoleBundle\Entity\User $utilisateur
     *
     * @return Commandes
     */
    public function setUtilisateur(\GestionPoleBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
    public function __construct()
    {

        // your own logic
        //$this->setEtatcomd("false");
        //$this->setEtatprod("false");
        //$this->setEtatqlt("false");
        //$this->setEtattech("false");
        //$this->setEtatlog("false");

    }

    /**
     * Set etatcomd
     *
     * @param boolean $etatcomd
     *
     * @return Commandes
     */
    public function setEtatcomd($etatcomd)
    {
        $this->etatcomd = $etatcomd;

        return $this;
    }

    /**
     * Get etatcomd
     *
     * @return boolean
     */
    public function getEtatcomd()
    {
        return $this->etatcomd;
    }

    /**
     * Set etatprod
     *
     * @param boolean $etatprod
     *
     * @return Commandes
     */
    public function setEtatprod($etatprod)
    {
        $this->etatprod = $etatprod;

        return $this;
    }

    /**
     * Get etatprod
     *
     * @return boolean
     */
    public function getEtatprod()
    {
        return $this->etatprod;
    }

    /**
     * Set etatqlt
     *
     * @param boolean $etatqlt
     *
     * @return Commandes
     */
    public function setEtatqlt($etatqlt)
    {
        $this->etatqlt = $etatqlt;

        return $this;
    }

    /**
     * Get etatqlt
     *
     * @return boolean
     */
    public function getEtatqlt()
    {
        return $this->etatqlt;
    }

    /**
     * Set etattech
     *
     * @param boolean $etattech
     *
     * @return Commandes
     */
    public function setEtattech($etattech)
    {
        $this->etattech = $etattech;

        return $this;
    }

    /**
     * Get etattech
     *
     * @return boolean
     */
    public function getEtattech()
    {
        return $this->etattech;
    }



    /**
     * Set prodfin
     *
     * @param boolean $prodfin
     *
     * @return Commandes
     */
    public function setProdfin($prodfin)
    {
        $this->prodfin = $prodfin;

        return $this;
    }

    /**
     * Get prodfin
     *
     * @return boolean
     */
    public function getProdfin()
    {
        return $this->prodfin;
    }

    /**
     * Set livAzs
     *
     * @param boolean $livAzs
     *
     * @return Commandes
     */
    public function setLivAzs($livAzs)
    {
        $this->liv_azs = $livAzs;

        return $this;
    }

    /**
     * Get livAzs
     *
     * @return boolean
     */
    public function getLivAzs()
    {
        return $this->liv_azs;
    }

    /**
     * Set prixDec
     *
     * @param integer $prixDec
     *
     * @return Commandes
     */
    public function setPrixDec($prixDec)
    {
        $this->prix_dec = $prixDec;

        return $this;
    }

    /**
     * Get prixDec
     *
     * @return int
     */
    public function getPrixDec()
    {
        return $this->prix_dec;
    }

    /**
     * Set ft
     *
     * @param integer $ft
     *
     * @return Commandes
     */
    public function setFt($ft)
    {
        $this->ft = $ft;

        return $this;
    }

    /**
     * Get ft
     *
     * @return int
     */
    public function getFt()
    {
        return $this->ft;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Commandes
     */
    public function setCommentaire($commentaire)
    {
        $this->Commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->Commentaire;
    }



  

    public function __toString() {
        return $this->numcompte;
    }


    /**
     * Add etiquette
     *
     * @param \GestionPoleBundle\Entity\Etiquette $etiquette
     *
     * @return Commandes
     */
    public function addEtiquette(\GestionPoleBundle\Entity\Etiquette $etiquette)
    {
        $this->etiquette[] = $etiquette;

        return $this;
    }

    /**
     * Remove etiquette
     *
     * @param \GestionPoleBundle\Entity\Etiquette $etiquette
     */
    public function removeEtiquette(\GestionPoleBundle\Entity\Etiquette $etiquette)
    {
        $this->etiquette->removeElement($etiquette);
    }

    /**
     * Get etiquette
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtiquette()
    {
        return $this->etiquette;
    }

    /**
     * Set refuse
     *
     * @param boolean $refuse
     *
     * @return Commandes
     */
    public function setRefuse($refuse)
    {
        $this->refuse = $refuse;

        return $this;
    }

    /**
     * Get refuse
     *
     * @return boolean
     */
    public function getRefuse()
    {
        return $this->refuse;
    }

    /**
     * Add palette
     *
     * @param \GestionPoleBundle\Entity\Palette $palette
     *
     * @return Commandes
     */
    public function addPalette(\GestionPoleBundle\Entity\Palette $palette)
    {
        $this->palette[] = $palette;

        return $this;
    }

    /**
     * Remove palette
     *
     * @param \GestionPoleBundle\Entity\Palette $palette
     */
    public function removePalette(\GestionPoleBundle\Entity\Palette $palette)
    {
        $this->palette->removeElement($palette);
    }

    /**
     * Get palette
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPalette()
    {
        return $this->palette;
    }



    /**
     * Set retouche
     *
     * @param boolean $retouche
     *
     * @return Commandes
     */
    public function setRetouche($retouche)
    {
        $this->retouche = $retouche;

        return $this;
    }

    /**
     * Get retouche
     *
     * @return boolean
     */
    public function getRetouche()
    {
        return $this->retouche;
    }

    /**
     * Add retiquette
     *
     * @param \GestionPoleBundle\Entity\Retiquette $retiquette
     *
     * @return Commandes
     */
    public function addRetiquette(\GestionPoleBundle\Entity\Retiquette $retiquette)
    {
        $this->retiquette[] = $retiquette;

        return $this;
    }

    /**
     * Remove retiquette
     *
     * @param \GestionPoleBundle\Entity\Retiquette $retiquette
     */
    public function removeRetiquette(\GestionPoleBundle\Entity\Retiquette $retiquette)
    {
        $this->retiquette->removeElement($retiquette);
    }

    /**
     * Get retiquette
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRetiquette()
    {
        return $this->retiquette;
    }

    /**
     * Add fabrication
     *
     * @param \GestionPoleBundle\Entity\Fabrication $fabrication
     *
     * @return Commandes
     */
    public function addFabrication(\GestionPoleBundle\Entity\Fabrication $fabrication)
    {
        $this->fabrication[] = $fabrication;

        return $this;
    }

    /**
     * Remove fabrication
     *
     * @param \GestionPoleBundle\Entity\Fabrication $fabrication
     */
    public function removeFabrication(\GestionPoleBundle\Entity\Fabrication $fabrication)
    {
        $this->fabrication->removeElement($fabrication);
    }

    /**
     * Get fabrication
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFabrication()
    {
        return $this->fabrication;
    }



    /**
     * Set marchandise
     *
     * @param boolean $marchandise
     *
     * @return Commandes
     */
    public function setMarchandise($marchandise)
    {
        $this->marchandise = $marchandise;

        return $this;
    }

    /**
     * Get marchandise
     *
     * @return boolean
     */
    public function getMarchandise()
    {
        return $this->marchandise;
    }

    /**
     * Set ecran
     *
     * @param boolean $ecran
     *
     * @return Commandes
     */
    public function setEcran($ecran)
    {
        $this->ecran = $ecran;

        return $this;
    }

    /**
     * Get ecran
     *
     * @return boolean
     */
    public function getEcran()
    {
        return $this->ecran;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return Commandes
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set traitement
     *
     * @param string $traitement
     *
     * @return Commandes
     */
    public function setTraitement($traitement)
    {
        $this->traitement = $traitement;

        return $this;
    }

    /**
     * Get traitement
     *
     * @return string
     */
    public function getTraitement()
    {
        return $this->traitement;
    }

    /**
     * Set conditionnement
     *
     * @param string $conditionnement
     *
     * @return Commandes
     */
    public function setConditionnement($conditionnement)
    {
        $this->conditionnement = $conditionnement;

        return $this;
    }

    /**
     * Get conditionnement
     *
     * @return string
     */
    public function getConditionnement()
    {
        return $this->conditionnement;
    }



    /**
     * Set refarticle
     *
     * @param \GestionPoleBundle\Entity\Stockage $refarticle
     *
     * @return Commandes
     */
    public function setRefarticle(\GestionPoleBundle\Entity\Stockage $refarticle = null)
    {
        $this->refarticle = $refarticle;

        return $this;
    }

    /**
     * Get refarticle
     *
     * @return \GestionPoleBundle\Entity\Stockage
     */
    public function getRefarticle()
    {
        return $this->refarticle;
    }



    /**
     * Set etatlog
     *
     * @param boolean $etatlog
     *
     * @return Commandes
     */
    public function setEtatlog($etatlog)
    {
        $this->etatlog = $etatlog;

        return $this;
    }

    /**
     * Get etatlog
     *
     * @return boolean
     */
    public function getEtatlog()
    {
        return $this->etatlog;
    }

    /**
     * Set misejour
     *
     * @param boolean $misejour
     *
     * @return Commandes
     */
    public function setMisejour($misejour)
    {
        $this->misejour = $misejour;

        return $this;
    }

    /**
     * Get misejour
     *
     * @return boolean
     */
    public function getMisejour()
    {
        return $this->misejour;
    }

    /**
     * Set numcomd
     *
     * @param string $numcomd
     *
     * @return Commandes
     */
    public function setNumcomd($numcomd)
    {
        $this->numcomd = $numcomd;

        return $this;
    }

    /**
     * Get numcomd
     *
     * @return string
     */
    public function getNumcomd()
    {
        return $this->numcomd;
    }

    

    /**
     * Set dateliv
     *
     * @param \DateTime $dateliv
     *
     * @return Commandes
     */
    public function setDateliv($dateliv)
    {
        $this->dateliv = $dateliv;

        return $this;
    }

    /**
     * Get dateliv
     *
     * @return \DateTime
     */
    public function getDateliv()
    {
        return $this->dateliv;
    }

    /**
     * Set dateprd
     *
     * @param \DateTime $dateprd
     *
     * @return Commandes
     */
    public function setDateprd($dateprd)
    {
        $this->dateprd = $dateprd;

        return $this;
    }

    /**
     * Get dateprd
     *
     * @return \DateTime
     */
    public function getDateprd()
    {
        return $this->dateprd;
    }

    /**
     * Set annee
     *
     * @param \DateTime $annee
     *
     * @return Commandes
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return \DateTime
     */
    public function getAnnee()
    {
        return $this->annee;
    }
	   /**
     * Set bl
     *
     * @param boolean $bl
     *
     * @return Commandes
     */
    public function setBl($bl)
    {
        $this->bl = $bl;

        return $this;
    }

    /**
     * Get bl
     *
     * @return boolean
     */
    public function getBl()
    {
        return $this->bl;
    }
	 /**
     * Set certifie
     *
     * @param boolean $certifie
     *
     * @return Commandes
     */
    public function setCertifie($certifie)
    {
        $this->certifie = $certifie;

        return $this;
    }

    /**
     * Get certifie
     *
     * @return boolean
     */
    public function getCertifie()
    {
        return $this->certifie;
    }
	
	 
	 /**
     * Set facture
     *
     * @param boolean $facture
     *
     * @return Commandes
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;

        return $this;
    }

    /**
     * Get facture
     *
     * @return boolean
     */
    public function getFacture()
    {
        return $this->facture;
    }
	    /**
     * Add fact
     *
     * @param \GestionPoleBundle\Entity\Facture $fact
     *
     * @return Commandes
     */
    public function addFact(\GestionPoleBundle\Entity\Facture $fact)
    {
        $this->fact[] = $fact;

        return $this;
    }

    /**
     * Remove fact
     *
     * @param \GestionPoleBundle\Entity\Facture $fact
     */
    public function removeFact(\GestionPoleBundle\Entity\Facture $fact)
    {
        $this->fact->removeElement($fact);
    }

    /**
     * Get fact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFact()
    {
        return $this->fact;
    }
	 /**
     * Set etiq
     *
     * @param boolean $etiq
     *
     * @return Commandes
     */
    public function setEtiq($etiq)
    {
        $this->etiq = $etiq;

        return $this;
    }

    /**
     * Get etiq
     *
     * @return boolean
     */
    public function getEtiq()
    {
        return $this->etiq;
    }

    /**
     * Set plt
     *
     * @param boolean $plt
     *
     * @return Commandes
     */
    public function setPlt($plt)
    {
        $this->plt = $plt;

        return $this;
    }

    /**
     * Get plt
     *
     * @return boolean
     */
    public function getPlt()
    {
        return $this->plt;
    }

    /**
     * Set retq
     *
     * @param boolean $retq
     *
     * @return Commandes
     */
    public function setRetq($retq)
    {
        $this->retq = $retq;

        return $this;
    }

    /**
     * Get retq
     *
     * @return boolean
     */
    public function getRetq()
    {
        return $this->retq;
    }

    /**
     * Set bat
     *
     * @param \GestionPoleBundle\Entity\Media $bat
     *
     * @return Commandes
     */
    public function setBat(\GestionPoleBundle\Entity\Media $bat = null)
    {
        $this->bat = $bat;

        return $this;
    }

    /**
     * Get bat
     *
     * @return \GestionPoleBundle\Entity\Media
     */
    public function getBat()
    {
        return $this->bat;
    }

    /**
     * Set eplt
     *
     * @param \GestionPoleBundle\Entity\Media $eplt
     *
     * @return Commandes
     */
    public function setEplt(\GestionPoleBundle\Entity\Media $eplt = null)
    {
        $this->eplt = $eplt;

        return $this;
    }

    /**
     * Get eplt
     *
     * @return \GestionPoleBundle\Entity\Media
     */
    public function getEplt()
    {
        return $this->eplt;
    }

    /**
     * Set cxsl
     *
     * @param \GestionPoleBundle\Entity\Media $cxsl
     *
     * @return Commandes
     */
    public function setCxsl(\GestionPoleBundle\Entity\Media $cxsl = null)
    {
        $this->cxsl = $cxsl;

        return $this;
    }

    /**
     * Get cxsl
     *
     * @return \GestionPoleBundle\Entity\Media
     */
    public function getCxsl()
    {
        return $this->cxsl;
    }

    /**
     * Set cpdf
     *
     * @param \GestionPoleBundle\Entity\Media $cpdf
     *
     * @return Commandes
     */
    public function setCpdf(\GestionPoleBundle\Entity\Media $cpdf = null)
    {
        $this->cpdf = $cpdf;

        return $this;
    }

    /**
     * Get cpdf
     *
     * @return \GestionPoleBundle\Entity\Media
     */
    public function getCpdf()
    {
        return $this->cpdf;
    }
}
