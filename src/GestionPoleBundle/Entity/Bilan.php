<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bilan
 *
 * @ORM\Table(name="bilan")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\BilanRepository")
 */
class Bilan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nompiece", type="string", length=255, nullable=true)
     */
    private $nompiece;

    /**
     * @var string
     *
     * @ORM\Column(name="qtentre", type="string", length=255, nullable=true)
     */
    private $qtentre;

    /**
     * @var string
     *
     * @ORM\Column(name="qtsort", type="string", length=255, nullable=true)
     */
    private $qtsort;

    /**
     * @var string
     *
     * @ORM\Column(name="crtcomplet", type="string", length=255, nullable=true)
     */
    private $crtcomplet;

    /**
     * @var string
     *
     * @ORM\Column(name="crtincomplet", type="string", length=255, nullable=true)
     */
    private $crtincomplet;

    /**
     * @var string
     *
     * @ORM\Column(name="nbrpalet", type="string", length=255, nullable=true)
     */
    private $nbrpalet;

    /**
     * @var string
     *
     * @ORM\Column(name="nompalet", type="string", length=255, nullable=true)
     */
    private $nompalet;

    /**
     * @var string
     *
     * @ORM\Column(name="prixdec", type="string", length=255, nullable=true)
     */
    private $prixdec;

    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="bilan")
     * @ORM\JoinColumn(nullable=true)
     */
    private $commande;
    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", nullable=true)
     */
    private $etat;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nompiece
     *
     * @param string $nompiece
     *
     * @return Bilan
     */
    public function setNompiece($nompiece)
    {
        $this->nompiece = $nompiece;

        return $this;
    }

    /**
     * Get nompiece
     *
     * @return string
     */
    public function getNompiece()
    {
        return $this->nompiece;
    }

    /**
     * Set qtentre
     *
     * @param string $qtentre
     *
     * @return Bilan
     */
    public function setQtentre($qtentre)
    {
        $this->qtentre = $qtentre;

        return $this;
    }

    /**
     * Get qtentre
     *
     * @return string
     */
    public function getQtentre()
    {
        return $this->qtentre;
    }

    /**
     * Set qtsort
     *
     * @param string $qtsort
     *
     * @return Bilan
     */
    public function setQtsort($qtsort)
    {
        $this->qtsort = $qtsort;

        return $this;
    }

    /**
     * Get qtsort
     *
     * @return string
     */
    public function getQtsort()
    {
        return $this->qtsort;
    }

    /**
     * Set crtcomplet
     *
     * @param string $crtcomplet
     *
     * @return Bilan
     */
    public function setCrtcomplet($crtcomplet)
    {
        $this->crtcomplet = $crtcomplet;

        return $this;
    }

    /**
     * Get crtcomplet
     *
     * @return string
     */
    public function getCrtcomplet()
    {
        return $this->crtcomplet;
    }

    /**
     * Set crtincomplet
     *
     * @param string $crtincomplet
     *
     * @return Bilan
     */
    public function setCrtincomplet($crtincomplet)
    {
        $this->crtincomplet = $crtincomplet;

        return $this;
    }

    /**
     * Get crtincomplet
     *
     * @return string
     */
    public function getCrtincomplet()
    {
        return $this->crtincomplet;
    }

    /**
     * Set nbrpalet
     *
     * @param string $nbrpalet
     *
     * @return Bilan
     */
    public function setNbrpalet($nbrpalet)
    {
        $this->nbrpalet = $nbrpalet;

        return $this;
    }

    /**
     * Get nbrpalet
     *
     * @return string
     */
    public function getNbrpalet()
    {
        return $this->nbrpalet;
    }

    /**
     * Set nompalet
     *
     * @param string $nompalet
     *
     * @return Bilan
     */
    public function setNompalet($nompalet)
    {
        $this->nompalet = $nompalet;

        return $this;
    }

    /**
     * Get nompalet
     *
     * @return string
     */
    public function getNompalet()
    {
        return $this->nompalet;
    }

    /**
     * Set prixdec
     *
     * @param string $prixdec
     *
     * @return Bilan
     */
    public function setPrixdec($prixdec)
    {
        $this->prixdec = $prixdec;

        return $this;
    }

    /**
     * Get prixdec
     *
     * @return string
     */
    public function getPrixdec()
    {
        return $this->prixdec;
    }

    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Bilan
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

  

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Bilan
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
