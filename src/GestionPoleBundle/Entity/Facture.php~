<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 *
 * @ORM\Table(name="facture")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\FactureRepository")
 */
class Facture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="fact")
     * @ORM\JoinColumn(nullable=true)
     */
    private $commande;
    /**
     * @var string
     *
     * @ORM\Column(name="num", type="string", length=255, nullable=true)
     */
    private $num;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datech", type="datetime", nullable=true)
     */
    private $datech;

    /**
     * @var string
     *
     * @ORM\Column(name="regl", type="string", length=255, nullable=true)
     */
    private $regl;

    /**
     * @var string
     *
     * @ORM\Column(name="notva", type="string", length=255, nullable=true)
     */
    private $notva;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer", nullable=true)
     */
    private $qte;

    /**
     * @var string
     *
     * @ORM\Column(name="modep", type="string", length=255, nullable=true)
     */
    private $modep;
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="transport", type="float", nullable=true)
     */
    private $transport;

    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", nullable=true)
     */
    private $etat;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param string $num
     *
     * @return Facture
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return string
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Facture
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set datech
     *
     * @param \DateTime $datech
     *
     * @return Facture
     */
    public function setDatech($datech)
    {
        $this->datech = $datech;

        return $this;
    }

    /**
     * Get datech
     *
     * @return \DateTime
     */
    public function getDatech()
    {
        return $this->datech;
    }

    /**
     * Set regl
     *
     * @param string $regl
     *
     * @return Facture
     */
    public function setRegl($regl)
    {
        $this->regl = $regl;

        return $this;
    }

    /**
     * Get regl
     *
     * @return string
     */
    public function getRegl()
    {
        return $this->regl;
    }

    /**
     * Set notva
     *
     * @param string $notva
     *
     * @return Facture
     */
    public function setNotva($notva)
    {
        $this->notva = $notva;

        return $this;
    }

    /**
     * Get notva
     *
     * @return string
     */
    public function getNotva()
    {
        return $this->notva;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     *
     * @return Facture
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return int
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set modep
     *
     * @param string $modep
     *
     * @return Facture
     */
    public function setModep($modep)
    {
        $this->modep = $modep;

        return $this;
    }

    /**
     * Get modep
     *
     * @return string
     */
    public function getModep()
    {
        return $this->modep;
    }

    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Facture
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Facture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set transport
     *
     * @param float $transport
     *
     * @return Facture
     */
    public function setTransport($transport)
    {
        $this->transport = $transport;

        return $this;
    }

    /**
     * Get transport
     *
     * @return float
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Facture
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return boolean
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
