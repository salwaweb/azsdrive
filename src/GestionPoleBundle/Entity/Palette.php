<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Palette
 *
 * @ORM\Table(name="palette")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\PaletteRepository")
 */
class Palette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var int
     *
     * @ORM\Column(name="entree", type="integer")
     */
    private $entree;

    /**
     * @var int
     *
     * @ORM\Column(name="sortie", type="integer")
     */
    private $sortie;

    /**
     * @var float
     *
     * @ORM\Column(name="rebus", type="float")
     */
    private $rebus;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="hauteurp", type="float")
     */
    private $hauteurp;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrp", type="integer")
     */
    private $nbrp;
    /**
     * @var int
     *
     * @ORM\Column(name="crt", type="integer")
     */
    private $crt;

    /**
     * @var float
     *
     * @ORM\Column(name="poid", type="float")
     */
    private $poid;


    /**
     * @var bool
     *
     * @ORM\Column(name="gerbable", type="boolean")
     */
    private $gerbable;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="palette")
     * @ORM\JoinColumn(nullable=true)
     */

    private $commande;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entree
     *
     * @param string $entree
     *
     * @return Palette
     */
    public function setEntree($entree)
    {
        $this->entree = $entree;

        return $this;
    }

    /**
     * Get entree
     *
     * @return string
     */
    public function getEntree()
    {
        return $this->entree;
    }

    /**
     * Set sortie
     *
     * @param integer $sortie
     *
     * @return Palette
     */
    public function setSortie($sortie)
    {
        $this->sortie = $sortie;

        return $this;
    }

    /**
     * Get sortie
     *
     * @return int
     */
    public function getSortie()
    {
        return $this->sortie;
    }

    /**
     * Set rebus
     *
     * @param float $rebus
     *
     * @return Palette
     */
    public function setRebus($rebus)
    {
        $this->rebus = $rebus;

        return $this;
    }

    /**
     * Get rebus
     *
     * @return float
     */
    public function getRebus()
    {
        return $this->rebus;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Palette
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set hauteurp
     *
     * @param float $hauteurp
     *
     * @return Palette
     */
    public function setHauteurp($hauteurp)
    {
        $this->hauteurp = $hauteurp;

        return $this;
    }

    /**
     * Get hauteurp
     *
     * @return float
     */
    public function getHauteurp()
    {
        return $this->hauteurp;
    }

    /**
     * Set nbrp
     *
     * @param integer $nbrp
     *
     * @return Palette
     */
    public function setNbrp($nbrp)
    {
        $this->nbrp = $nbrp;

        return $this;
    }

    /**
     * Get nbrp
     *
     * @return int
     */
    public function getNbrp()
    {
        return $this->nbrp;
    }
    /**
     * Set crt
     *
     * @param integer $crt
     *
     * @return Palette
     */
    public function setCrt($crt)
    {
        $this->crt = $crt;

        return $this;
    }

    /**
     * Get crt
     *
     * @return int
     */
    public function getCrt()
    {
        return $this->crt;
    }
    /**
     * Set poid
     *
     * @param float $poid
     *
     * @return Palette
     */
    public function setPoid($poid)
    {
        $this->poid = $poid;

        return $this;
    }

    /**
     * Get poid
     *
     * @return float
     */
    public function getPoid()
    {
        return $this->poid;
    }

    /**
     * Set gerbable
     *
     * @param boolean $gerbable
     *
     * @return Palette
     */
    public function setGerbable($gerbable)
    {
        $this->gerbable = $gerbable;

        return $this;
    }

    /**
     * Get gerbable
     *
     * @return bool
     */
    public function getGerbable()
    {
        return $this->gerbable;
    }

    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Palette
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Palette
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }
}
