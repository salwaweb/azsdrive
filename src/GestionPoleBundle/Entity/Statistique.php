<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statistique
 *
 * @ORM\Table(name="statistique")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\StatistiqueRepository")
 */
class Statistique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="annee", type="date")
     */
    private $annee;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrcomd", type="integer")
     */
    private $nbrcomd;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrpiece", type="integer")
     */
    private $nbrpiece;

    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", inversedBy="statistique")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee
     *
     * @param \DateTime $annee
     *
     * @return Statistique
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return \DateTime
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set nbrcomd
     *
     * @param integer $nbrcomd
     *
     * @return Statistique
     */
    public function setNbrcomd($nbrcomd)
    {
        $this->nbrcomd = $nbrcomd;

        return $this;
    }

    /**
     * Get nbrcomd
     *
     * @return int
     */
    public function getNbrcomd()
    {
        return $this->nbrcomd;
    }

    /**
     * Set nbrpiece
     *
     * @param integer $nbrpiece
     *
     * @return Statistique
     */
    public function setNbrpiece($nbrpiece)
    {
        $this->nbrpiece = $nbrpiece;

        return $this;
    }

    /**
     * Get nbrpiece
     *
     * @return int
     */
    public function getNbrpiece()
    {
        return $this->nbrpiece;
    }

    /**
     * Set utilisateur
     *
     * @param \GestionPoleBundle\Entity\User $utilisateur
     *
     * @return Statistique
     */
    public function setUtilisateur(\GestionPoleBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
