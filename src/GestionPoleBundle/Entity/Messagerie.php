<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messagerie
 *
 * @ORM\Table(name="messagerie")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\MessagerieRepository")
 */
class Messagerie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sujet", type="string", length=255)
     */
    private $sujet;



    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */

    private $pole;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\User", inversedBy="messagerie")
     * @ORM\JoinColumn(nullable=true)
     */

    private $utilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sujet
     *
     * @param string $sujet
     *
     * @return Messagerie
     */
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * Get sujet
     *
     * @return string
     */
    public function getSujet()
    {
        return $this->sujet;
    }



    /**
     * Set message
     *
     * @param string $message
     *
     * @return Messagerie
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }



    /**
     * Set pole
     *
     * @param \GestionPoleBundle\Entity\User $pole
     *
     * @return Messagerie
     */
    public function setPole(\GestionPoleBundle\Entity\User $pole)
    {
        $this->pole = $pole;

        return $this;
    }

    /**
     * Get pole
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getPole()
    {
        return $this->pole;
    }

    /**
     * Set utilisateur
     *
     * @param \GestionPoleBundle\Entity\User $utilisateur
     *
     * @return Messagerie
     */
    public function setUtilisateur(\GestionPoleBundle\Entity\User $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \GestionPoleBundle\Entity\User
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
