<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Etiquette
 *
 * @ORM\Table(name="etiquette")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\EtiquetteRepository")
 */
class Etiquette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	  /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private  $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrpiece", type="integer")
     */
    private $nbrpiece;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrcarton", type="integer")
     */
    private $nbrcarton;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrplt", type="integer")
     */
    private $nbrplt;

    /**
     * @var int
     *
     * @ORM\Column(name="crtincomplet", type="integer")
     */
    private $crtincomplet;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total;

    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="etiquette")
     * @ORM\JoinColumn(nullable=true)
     */

    private $commande;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
 /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Etiquette
     */
    public function setNom($nom)
    {
        $this->prenom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Set nbrpiece
     *
     * @param integer $nbrpiece
     *
     * @return Etiquette
     */
    public function setNbrpiece($nbrpiece)
    {
        $this->nbrpiece = $nbrpiece;

        return $this;
    }

    /**
     * Get nbrpiece
     *
     * @return int
     */
    public function getNbrpiece()
    {
        return $this->nbrpiece;
    }

    /**
     * Set nbrcarton
     *
     * @param integer $nbrcarton
     *
     * @return Etiquette
     */
    public function setNbrcarton($nbrcarton)
    {
        $this->nbrcarton = $nbrcarton;

        return $this;
    }

    /**
     * Get nbrcarton
     *
     * @return int
     */
    public function getNbrcarton()
    {
        return $this->nbrcarton;
    }

    /**
     * Set nbrplt
     *
     * @param integer $nbrplt
     *
     * @return Etiquette
     */
    public function setNbrplt($nbrplt)
    {
        $this->nbrplt = $nbrplt;

        return $this;
    }

    /**
     * Get nbrplt
     *
     * @return int
     */
    public function getNbrplt()
    {
        return $this->nbrplt;
    }

    /**
     * Set crtincomplet
     *
     * @param integer $crtincomplet
     *
     * @return Etiquette
     */
    public function setCrtincomplet($crtincomplet)
    {
        $this->crtincomplet = $crtincomplet;

        return $this;
    }

    /**
     * Get crtincomplet
     *
     * @return int
     */
    public function getCrtincomplet()
    {
        return $this->crtincomplet;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Etiquette
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }









    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Etiquette
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }
}
