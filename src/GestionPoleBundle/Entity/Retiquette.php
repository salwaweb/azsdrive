<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Retiquette
 *
 * @ORM\Table(name="retiquette")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\RetiquetteRepository")
 */
class Retiquette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="string", length=255)
     */
    private  $total;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    private $tel;
    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="retiquette")
     * @ORM\JoinColumn(nullable=true)
     */

    private $commande;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

  

    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Retiquette
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Retiquette
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Retiquette
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Retiquette
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return Retiquette
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }
}
