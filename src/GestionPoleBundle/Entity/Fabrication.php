<?php

namespace GestionPoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fabrication
 *
 * @ORM\Table(name="fabrication")
 * @ORM\Entity(repositoryClass="GestionPoleBundle\Repository\FabricationRepository")
 */
class Fabrication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomdec", type="string", length=255)
     */
    private $nomdec;

    /**
     * @var string
     *
     * @ORM\Column(name="jour", type="string", length=255)
     */
    private $jour;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrpasse", type="integer")
     */
    private $nbrpasse;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="string", length=255)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="GestionPoleBundle\Entity\Commandes", inversedBy="fabrication")
     * @ORM\JoinColumn(nullable=true)
     */

    private $commande;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomdec
     *
     * @param string $nomdec
     *
     * @return Fabrication
     */
    public function setNomdec($nomdec)
    {
        $this->nomdec = $nomdec;

        return $this;
    }

    /**
     * Get nomdec
     *
     * @return string
     */
    public function getNomdec()
    {
        return $this->nomdec;
    }

   

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Fabrication
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nbrpasse
     *
     * @param integer $nbrpasse
     *
     * @return Fabrication
     */
    public function setNbrpasse($nbrpasse)
    {
        $this->nbrpasse = $nbrpasse;

        return $this;
    }

    /**
     * Get nbrpasse
     *
     * @return int
     */
    public function getNbrpasse()
    {
        return $this->nbrpasse;
    }



    /**
     * Set quantite
     *
     * @param string $quantite
     *
     * @return Fabrication
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set commande
     *
     * @param \GestionPoleBundle\Entity\Commandes $commande
     *
     * @return Fabrication
     */
    public function setCommande(\GestionPoleBundle\Entity\Commandes $commande = null)
    {
        $this->commande = $commande;

        return $this;
    }

    /**
     * Get commande
     *
     * @return \GestionPoleBundle\Entity\Commandes
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * Set jour
     *
     * @param string $jour
     *
     * @return Fabrication
     */
    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }

    /**
     * Get jour
     *
     * @return string
     */
    public function getJour()
    {
        return $this->jour;
    }
}
