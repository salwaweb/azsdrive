<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     */
    public function indexAction()
    {
        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles NOT LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%')
                ->andWhere('u.roles LIKE :role2')
                ->setParameter('role2', '%"ROLE_TECHNIQUE"%')
                ->orWhere('u.roles LIKE :role3')
                ->setParameter('role3', '%"ROLE_RECEPTION"%')
                ->orWhere('u.roles LIKE :role4')
                ->setParameter('role4', '%"ROLE_PRODUCTION"%')
                ->orWhere('u.roles LIKE :role6')
                ->setParameter('role6', '%"ROLE_LOGISTIQUE"%')
                ->orWhere('u.roles LIKE :role7')
                ->setParameter('role7', '%"ROLE_QUALITE"%')
                ->orWhere('u.roles LIKE :role8')
                ->setParameter('role8', '%"ROLE_SUPER_ADMIN"%');


            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:admin:GestionPole/liste.html.twig', array(
                'users' => $users,

            ));
        }
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */

    public function clientAction()
    {
        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles NOT LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%')
                ->andWhere('u.roles NOT LIKE :role2')
                ->setParameter('role2', '%"ROLE_TECHNIQUE"%')
                ->andWhere('u.roles NOT LIKE :role4')
                ->setParameter('role4', '%"ROLE_PRODUCTION"%')
                ->andWhere('u.roles NOT LIKE :role5')
                ->setParameter('role5', '%"ROLE_RECEPTION"%')
                ->andWhere('u.roles NOT LIKE :role6')
                ->setParameter('role6', '%"ROLE_LOGISTIQUE"%')
                ->andWhere('u.roles NOT LIKE :role7')
                ->setParameter('role7', '%"ROLE_QUALITE"%')
                ->andWhere('u.roles NOT LIKE :role8')
                ->setParameter('role8', '%"ROLE_SUPER_ADMIN"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:admin:GestionClient/listeclient.html.twig', array(
                'users' => $users,

            ));
        }
    }

    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('GestionPoleBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('GestionPoleBundle:admin:GestionPole/ajoutnv.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
    /**
     * Creates a new user entity.
     *
     */
    public function newclientAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('GestionPoleBundle\Form\ClientType', $user);
        $form->handleRequest($request);
        //$random = random_int(1, 99999);
        //dd($random);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('list_client');
        }

        return $this->render('GestionPoleBundle:admin:GestionClient/ajoutnvcl.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));


    }
	
	
    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('GestionPoleBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('GestionPoleBundle:admin:GestionPole/edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction(Request $request,User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('GestionPoleBundle\Form\Client1Type', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('list_client');
        }

        return $this->render('GestionPoleBundle:admin:GestionClient/edit.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
            'edit_form' => $editForm->createView(),
        ));
    }
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function vldclientAction()
    {
        if (true == $this->get('security.authorization_checker')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:admin:GestionClient/vlidclient.html.twig', array(
                'users' => $users,

            ));
        }
    }
    public function clientrAction()
    {
        if (true == $this->get('security.authorization_checker')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:pole-commande:Client/liste-client.html.twig', array(
                'users' => $users,

            ));
        }
    }

    public function clientpAction()
    {
        if (true == $this->get('security.authorization_checker')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:pole-production:Client/liste-client.html.twig', array(
                'users' => $users,

            ));
        }
    }

    public function clientlAction()
    {
        if (true == $this->get('security.authorization_checker')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:pole-logistique:Client/liste-client.html.twig', array(
                'users' => $users,

            ));
        }
    }
    public function clientqAction()
    {
        if (true == $this->get('security.authorization_checker')) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->createQueryBuilder();
            $qb->select('u')
                ->from('GestionPoleBundle:User', 'u')
                ->addOrderBy('u.id', 'DESC')
                ->where('u.roles LIKE :role')
                ->setParameter('role', '%"ROLE_CLIENT"%');
            // ->setParameter('role','%"ROLE_PRODUCTION"%');
            $query = $qb->getQuery();
            $users = $query->getResult();

            //dd($users);
            return $this->render('GestionPoleBundle:pole-qualite:Client/liste-client.html.twig', array(
                'users' => $users,

            ));
        }
    }


    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
