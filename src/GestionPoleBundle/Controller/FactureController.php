<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Facture;
use GestionPoleBundle\Entity\Commandes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Facture controller.
 *
 */
class FactureController extends Controller
{




    public function poleAction()
    {


        return $this->render('GestionPoleBundle:pole-facturation:index.html.twig');
    }
    public function gestfactAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatqlt' => 1, 'etatlog' => 0, 'facture'=> null)
        );
        //dd($bilan);

        return $this->render('GestionPoleBundle:pole-facturation/Commandes:gestfact.html.twig', array(

            'commandes'=>$commandes
        ));
    }

    public function creefactAction(Commandes $commande, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $facture = new Facture();
        $form = $this->createForm('GestionPoleBundle\Form\FactureType', $facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $numero = random_int(0000001, 9999999);
            $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commande));
            $cmd = $comd->getId();
            $qt = $comd->getQtecdee();
            $vld = $comd->setFacture(true);
            //dd($vld);
            $facture->setDate(new \DateTime());
            $facture->setNum($numero);
            $facture->setQte($qt);
            $facture->setCommande($commande);
            //dd($facture);
            $em->persist($facture, $vld);
            $em->flush();

            return $this->redirectToRoute('gestion-fact');
        }


        return $this->render('GestionPoleBundle:pole-facturation/Commandes:creefact.html.twig', array(
            'facture' => $facture,
            'form' => $form->createView(),
        ));
    }
    public function listefactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $factures = $em->getRepository('GestionPoleBundle:Facture')->findBy(
            array('etat' => 1 )
        );

        return $this->render('GestionPoleBundle:pole-facturation/Commandes:liste-factures.html.twig', array(
            'factures' => $factures,
        ));

    }
    public function nonfactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $factures = $em->getRepository('GestionPoleBundle:Facture')->findBy(
            array('etat' => null )
        );

        return $this->render('GestionPoleBundle:pole-facturation/Commandes:nonfact.html.twig', array(
            'factures' => $factures,
        ));

    }
    public function factureAction(Facture $facture)
    {
        $em = $this->getDoctrine()->getManager();
        $mt=0;
        $tva=0;
        $ttc=0;
        $cmpte=0;
        $net=0;

        $html = $this->renderView('GestionPoleBundle:pole-facturation:Commandes/Facture.html.twig',
            array(
                'facture' => $facture,
                'mt' => $mt,
                'tva' => $tva,
                'ttc' => $ttc,
                'cmpte' => $cmpte,
                'net' => $net
            )
        );

        $html2pdf = new Html2Pdf('P','A4','fr');
        $html2pdf->pdf->SetAuthor('Facture');
        $html2pdf->pdf->SetTitle('Facture');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('Facture.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
    public function facturationAction(Request $request, Facture $id)
    {


        $em= $this->getDoctrine()->getManager();

        $fact = $em->getRepository('GestionPoleBundle:Facture')->findOneBy(array('id' => $id));

        $fact->setEtat(true);

        $em->persist($fact); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('list-facture');

    }

    public function gestblAction()
    {
        $em = $this->getDoctrine()->getManager();
        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();
        $bilan= $em->getRepository('GestionPoleBundle:Bilan')->findAll();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        //dd($bilan);

        return $this->render('GestionPoleBundle:pole-facturation/Commandes:gestbl.html.twig', array(
            'palettes' => $palettes,
            'bilan' => $bilan,
            'etiquettes' => $etiquettes,
            'commandes'=>$commandes
        ));
    }
    /**
     * Lists all facture entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $factures = $em->getRepository('GestionPoleBundle:Facture')->findAll();

        return $this->render('facture/index.html.twig', array(
            'factures' => $factures,
        ));
    }

    /**
     * Creates a new facture entity.
     *
     */
    public function newAction(Request $request)
    {
        $facture = new Facture();
        $form = $this->createForm('GestionPoleBundle\Form\FactureType', $facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($facture);
            $em->flush();

            return $this->redirectToRoute('facture_show', array('id' => $facture->getId()));
        }

        return $this->render('facture/new.html.twig', array(
            'facture' => $facture,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a facture entity.
     *
     */
    public function showAction(Facture $facture)
    {
        $deleteForm = $this->createDeleteForm($facture);

        return $this->render('facture/show.html.twig', array(
            'facture' => $facture,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing facture entity.
     *
     */
    public function editAction(Request $request, Facture $facture)
    {
        $deleteForm = $this->createDeleteForm($facture);
        $editForm = $this->createForm('GestionPoleBundle\Form\FactureType', $facture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('facture_edit', array('id' => $facture->getId()));
        }

        return $this->render('facture/edit.html.twig', array(
            'facture' => $facture,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a facture entity.
     *
     */
    public function deleteAction(Request $request, Facture $facture)
    {
        $form = $this->createDeleteForm($facture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($facture);
            $em->flush();
        }

        return $this->redirectToRoute('facture_index');
    }

    /**
     * Creates a form to delete a facture entity.
     *
     * @param Facture $facture The facture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Facture $facture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facture_delete', array('id' => $facture->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
