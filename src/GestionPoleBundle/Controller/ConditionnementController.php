<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Stockage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Entity\Bilan;
use GestionPoleBundle\Form\Stockage1Type;
use GestionPoleBundle\Entity\Etiquette;
use GestionPoleBundle\Entity\Palette;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Component\HttpFoundation\Session\Session;


use Symfony\Component\HttpFoundation\Request;

class ConditionnementController extends Controller
{
    public function indexAction()
    {
        return $this->render('GestionPoleBundle:pole-conditionement:index.html.twig', array(
            // ...
        ));
    }

    public function creerblAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array( 'etiq' => 1, 'plt' => 1, 'retq' => 1)
        );

        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
		$bilan= $em->getRepository('GestionPoleBundle:Bilan')->findAll();
        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();
      
        // dd($form);
        return $this->render('GestionPoleBundle:pole-conditionement:liste-comd.html.twig', array(
            'commandes' => $commandes,
			'bilan' => $bilan,
			'palettes' => $palettes,
                'etiquettes' => $etiquettes,

        ));

    }

    public function BLPDFAction(bilan $bilan)
    {
        $em = $this->getDoctrine()->getManager();
		
        $html = $this->renderView('GestionPoleBundle:pole-conditionement:BLPDF.html.twig',
            array(
                'bilan' => $bilan,
				
            )
        );

        $html2pdf = new Html2Pdf('P','A4','fr');
        $html2pdf->pdf->SetAuthor('DevAndClick');
        $html2pdf->pdf->SetTitle('BL ');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('BL.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
   public function creationblAction(Commandes $commande, Request $request)
    {
      
       //dd($nbrplt);
	   
	   $em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commande));
	    $cmd=$comd->getId();
		$numpiece=$comd->getNumpiece();
		$nompiece=$comd->getDesignation();
		$client=$comd->getUtilisateur();
		$prixdc=$comd->getPrixDec();
		 //$comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commande));

        $comd->setBl(true);
		//dd($client);
		 $user = $em->getRepository('GestionPoleBundle:User')->findOneBy(array('id' => $client));
		  $cl=$user->getId();
		 // dd($cl);
		 $plt = $em->getRepository('GestionPoleBundle:Palette')->findOneBy(array('commande' => $cmd));
		 
		$entree=$plt->getentree();
		$sortie=$plt->getsortie();
		$nbrplt=$plt->getNbrp();

        $crt=$plt->getCrt();
		$etq = $em->getRepository('GestionPoleBundle:Etiquette')->findOneBy(array('commande' => $cmd));
		$nbrcarton= 0;
		$nomp=null;
		
		 
	   $bilan = new Bilan();
        $bilan ->setCommande($commande);
        //$bilan ->setUtilisateur($cl);
        $bilan ->setNompiece($nompiece);
		$bilan ->setQtentre($entree);
        $bilan ->setQtsort($sortie);
        $bilan ->setNbrpalet($nbrplt);
        $bilan ->setNompalet($nomp);
        $bilan ->setCrtcomplet($nbrcarton);
        $bilan ->setCrtincomplet($crt);
        $bilan ->setPrixdec($prixdc);
		$em = $this->getDoctrine()->getManager();
        $em->persist($bilan,$comd);
        $em->flush();
//dd($access);
		
		
		
		
         return $this->redirectToRoute('creer-bl');

     
    }

}
