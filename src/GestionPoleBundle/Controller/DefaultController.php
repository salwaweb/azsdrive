<?php

namespace GestionPoleBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\User;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Form\CommandType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{


    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function clientAction(Request $request)
    {

        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_CLIENT')) {
            $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
            //dd($utilisateur);
            return $this->render('GestionPoleBundle:client:client.html.twig', array(
                'utilisateur' => $utilisateur,));
        }
        return $this->render('GestionPoleBundle:Default:index.html.twig');
    }




    /**
     * @Route("/production/", name="visitor_page")
     */
    public function VisitorAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('GestionPoleBundle:Default:produit.html.twig', []);
    }

    /**
     * @Route("/creer-comd/", name="creer-comod")
     */
    public function comodAction(Request $request)

    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findBy(
            array('utilisateur' => $utilisateur)
        );
        
        $etat=0;
        $mise=0;
        $numero = mt_rand(000000001, 999999999);

        $commande = new Commandes();

        $form = $this->createForm('GestionPoleBundle\Form\CommandesType', $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			
            $commande->setUtilisateur($utilisateur);
            $commande->setNumcomd($numero);
            $commande->setEtatlog($etat);
            $commande->setMisejour($mise);
            $commande->setAnnee(new \DateTime());
//dd($commande);

            $em->persist($commande);

            $em->flush($commande);

            return $this->redirectToRoute('suivi-comd', array('id' => $commande->getId()));
        }

        return $this->render('GestionPoleBundle:client:passer-commande.html.twig', array(
            'utilisateur' => $utilisateur,
            'commande' => $commande,
            'stockages' => $stockages,
            'form' => $form->createView(),
        ));

    }

    public function suiviAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        // $idUtilisateur =getUtilisateur($utilisateur)->getId();

        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur, 'etatcomd' => 1)
        );
        $commande = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur, 'refuse' => 1)
        );
        // dd($commandes);
        return $this->render('GestionPoleBundle:client:suivi.html.twig', array(
            'commandes' => $commandes,
            'commande' => $commande,
            'utilisateur' => $utilisateur,
        ));

    }
	 public function suivifactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        // $idUtilisateur =getUtilisateur($utilisateur)->getId();

        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur, 'facture' => 1)
        );
        $factures = $em->getRepository('GestionPoleBundle:Facture')->findAll();
        // dd($commandes);
        return $this->render('GestionPoleBundle:client:suivi-fact.html.twig', array(
            'commandes' => $commandes,
            'factures' => $factures,
            'utilisateur' => $utilisateur,
        ));

    }
    public function alert11Action()
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $qb = $em->createQueryBuilder();
        $qb->select('COUNT(c)')
            ->from('GestionPoleBundle:Commandes', 'c')
            ->where('c.dateliv < c.dateprd');
        $query = $qb->getQuery();

        $alert = $query->getResult();

        dd($alert);
        return $this->render('GestionPoleBundle:espace-client:alert.html.twig', array(

            'alert' => $alert,
            'utilisateur' => $utilisateur
        ));
    }
    public function alertAction()
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy( array(
            'utilisateur' => $utilisateur,
            'misejour' => 0
        ));
        return $this->render('GestionPoleBundle:client:alert.html.twig', array(
             'commandes'=>$commandes,
            'utilisateur' => $utilisateur
        ));
    }
    public function alerteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur)
        );
        // dd($commandes);

        return $this->render('GestionPoleBundle:client:alerte.html.twig', array(

            'commandes'=>$commandes,
            'utilisateur' => $utilisateur,
        ));
    }
    public function refuserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        // $idUtilisateur =getUtilisateur($utilisateur)->getId();

        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('utilisateur' => $utilisateur, 'refuse' => 1)
        );

        // dd($commandes);
        return $this->render('GestionPoleBundle:client:refuse-comd.html.twig', array(
            'commandes' => $commandes,

            'utilisateur' => $utilisateur,
        ));

    }
    public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();


        return $this->render('GestionPoleBundle:client:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function supportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $sav = new Sav();
        $form = $this->createForm('GestionPoleBundle\Form\SavType', $sav);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sav);
            $em->flush();

            return $this->redirectToRoute('fin-ticket', array('id' => $sav->getId()));
        }

        return $this->render('GestionPoleBundle:client:support.html.twig', array(
            'savs' => $savs,
            'sav' => $sav,
            'form' => $form->createView(),
        ));


    }
    public function finticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:client:show-sav.html.twig', array(
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,
            'utilisateur' => $utilisateur,

        ));
    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:client:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }
    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        //$utilisateur->getSupport($sav)->getReference();
        //dd($utilisateur);

        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));
        //dd($tikets);
        $msg=$request->get('message');
        $sup=$sav;
        //dd($sup);
        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
         $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-client');

    }

    public function rechercherAction(Request $request)
    {


        if ($request->isXmlHttpRequest()) {
            $commantaire = '';

            $commantaire = $request->request->get('commantaire');
            dd($commantaire);




        $em = $this->container->get('doctrine')->getEntityManager();



            return $this->render('GestionPoleBundle:pole-commande:Commandes/valid-commande.html.twig');
        }
       

    }
}
