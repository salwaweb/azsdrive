<?php

namespace GestionPoleBundle\Controller;
use GestionPoleBundle\Entity\Facture;
use GestionPoleBundle\Entity\Stockage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Form\Stockage1Type;
use GestionPoleBundle\Entity\Etiquette;
use GestionPoleBundle\Entity\Palette;
use Spipu\Html2Pdf\Html2Pdf;
use Symfony\Component\HttpFoundation\Session\Session;


use Symfony\Component\HttpFoundation\Request;

class LogistiqueController extends Controller
{
    public function indexAction()
    {
        return $this->render('GestionPoleBundle:pole-logistique:index.html.twig', array(
            // ...
        ));
    }

    public function recuprAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatqlt' => 1, 'etatlog' => 0)
        );


        // dd($form);
        return $this->render('GestionPoleBundle:pole-logistique/Commandes:liste-comd-prod.html.twig', array(
            'commandes' => $commandes,

        ));

    }


    public function recuAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-logistique/Commandes:liste-commande.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function accepterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatqlt' => 1)
        );

        return $this->render('GestionPoleBundle:pole-logistique/Commandes:commande-accepter.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function BLPDFAction(Commandes $commande)
    {
		 $em = $this->getDoctrine()->getManager();
        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();
        $html = $this->renderView('GestionPoleBundle:pole-logistique:Commandes/BLPDF.html.twig',
            array(
                'palettes' => $palettes,
                'etiquettes' => $etiquettes,
                'commande' => $commande
            )
        );
       
        $html2pdf = new Html2Pdf('P','A4','fr');
        $html2pdf->pdf->SetAuthor('TEST');
        $html2pdf->pdf->SetTitle('TEST');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('BL.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
    public function gestblAction()
    {
        $em = $this->getDoctrine()->getManager();
        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();
        $bilan= $em->getRepository('GestionPoleBundle:Bilan')->findAll();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        //dd($bilan);

        return $this->render('GestionPoleBundle:pole-logistique/Commandes:gestbl.html.twig', array(
            'palettes' => $palettes,
            'bilan' => $bilan,
            'etiquettes' => $etiquettes,
            'commandes'=>$commandes
        ));
    }
    public function etiquetteAction(Commandes $commandes,Request $request)
    {
        //By Saber for test
        //var_dump($request->get('nbrpiece'));die();
        $numero = random_int(0001, 9999);
        $ent=$request->get('entree');

        $srt=$request->get('sortie');
        $rebus=$request->get('rebus');
        $tp=$request->get('type');
        $hp=$request->get('hauteurp');
        $np=$request->get('nbrp');
        $poid=$request->get('poid');

        $crt=$request->get('crt');
        $gerbable=$request->get('gerbable');
		
		$em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commandes));
		$comd->setEtiq(true);
		
        $palette = new Palette();
        $palette ->setCommande($commandes);
        $palette->setEntree($ent);
        $palette->setNumero($numero);
        $palette->setSortie($srt);
        //$tt=($ent-$srt);
        $palette->setRebus($rebus );
        $palette->setType($tp);
        $palette->setHauteurp($hp);
        $palette->setNbrp($np);
        $palette->setPoid($poid);
        $palette->setCrt($crt);
        $palette->setGerbable($gerbable);


        $em = $this->getDoctrine()->getManager();
        $em->persist($palette, $comd);
        $em->flush();
        /*$session = new Session();
        $session->start();*/
        // add flash messages
        /*$session->getFlashBag()->add(
        'success',
        'ticket ajouté avec succés'
        );*/

        return $this->redirectToRoute('gestion-etiquette');



        //END by Saber for test


    }
    public function gestionetiquetteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        //dd($etiquettes);
        // dd($form);
        return $this->render('GestionPoleBundle:pole-logistique/Commandes:liste-etiquette.html.twig', array(
            'palettes' => $palettes,

        ));

    }
    public function etqtpdfAction(Palette $palette)
    {
        //return $this->render('GestionPoleBundle:pole-logistique:Commandes/Etqt-pdf.html.twig', array('etiquette' => $etiquette)); die();

        $html = $this->renderView('GestionPoleBundle:pole-logistique:Commandes/Etqt-pdf.html.twig',
            array('palette' => $palette));
        //var_dump($html);die();
        $html2pdf = new Html2Pdf('P','A7','fr');
        $html2pdf->pdf->SetAuthor('Etiquette');
        $html2pdf->pdf->SetTitle('Etiquette ');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('Etiquette.pdf');


        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
    public function misejourAction(Request $request, Commandes $id)
    {
        $qtcmd=$request->get('qtecdee');
        $qtstck=$request->get('qtstock');
        $qte=$qtstck-$qtcmd;

      $em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $id));

        $comd->setMisejour(true);
        $ref=$comd->getRefarticle();

        $stock = $em->getRepository('GestionPoleBundle:Stockage')->findOneBy(array('id' => $ref));
        $nvqte=$stock->setQuantite($qte);
        $em->persist($nvqte, $comd); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('comd-prod-reçu');

      }
	  public function marchandiseAction(Request $request, Commandes $id)
    {
    

      $em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $id));

        $comd->setMarchandise(true);

        $em->persist($comd); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('comd-prod-reçu');

      }

    public function stockAction()
    {
        $em = $this->getDoctrine()->getManager();
        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findAll();
        //dd($etiquettes);
        // dd($form);
        return $this->render('GestionPoleBundle:pole-logistique/Commandes:list-stock.html.twig', array(
            'stockages'=> $stockages,


        ));




    }

    public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-logistique:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:pole-logistique:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }

    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $msg=$request->get('message');

        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
        $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-logistique');

    }

}
