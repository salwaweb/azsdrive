<?php

namespace GestionPoleBundle\Controller;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Entity\Bilan;
use GestionPoleBundle\Entity\Commandes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Commande controller.
 *
 */
class CommandesController extends Controller
{



    /**
     * Lists all commande entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();

        return $this->render('commandes/index.html.twig', array(
            'commandes' => $commandes,
        ));
    }

    /**
     * Creates a new commande entity.
     *
     */
    public function newAction(Request $request)
    {
        $commande = new Commandes();
        $form = $this->createForm('GestionPoleBundle\Form\CommandesType', $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush($commande);

            return $this->redirectToRoute('commandes_show', array('id' => $commande->getId()));
        }

        return $this->render('commandes/new.html.twig', array(
            'commande' => $commande,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function vldadmAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande1Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste-commandes');
        }
        //dd($editForm);
        return $this->render('GestionPoleBundle:admin/GestionCommandes:valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function showAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande1Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste-commandes');
        }
        //dd($editForm);
        return $this->render('GestionPoleBundle:admin/GestionCommandes:show-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a commande entity.
     *
     */
	    public function validationblAction(bilan $bilan ,Request $request)
		{ 

		$em= $this->getDoctrine()->getManager();

        $bl = $em->getRepository('GestionPoleBundle:Bilan')->findOneBy(array('id' => $bilan));

        $bl->setEtat(true);
        
        $em->persist($bl); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('gestionbl');

      }

		
	
    public function validcomdAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande1Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste-commande');
        }
        //dd($editForm);
        return $this->render('GestionPoleBundle:pole-commande:Commandes/valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a commande entity.
     *
     */
    public function showcomdAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande1Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liste-commande');
        }
        //dd($editForm);
        return $this->render('GestionPoleBundle:pole-commande:Commandes/show-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a commande entity.
     *
     */
    public function vldtecAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\CommandeTechType', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commande-tech-recu');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-technique:valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Finds and displays a commande entity.
     *
     */
    public function validprodAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande2Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ordre-fabrication');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-production:Commandes/valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function validretoucheAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande5Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('retouche-encours');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-production:Commandes/valid-retouche.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function validretchqltAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande6Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('retouche-fin');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-qualite:Commandes/valid-retouche.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function showprodAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande2Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ordre-fabrication');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-production:Commandes/show-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function terminecomdAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\CommandePfType', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commande-termine');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-production:Commandes/termine-comd.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * valisation commande port qualite
     *
     */
    public function validqltAction(Commandes $commande ,Request $request)
    {
        $em= $this->getDoctrine()->getManager();
        $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commande));


        $comd->setDateprd(new \DateTime('now +3 days'));
        $em->persist( $comd);

        $editForm = $this->createForm('GestionPoleBundle\Form\Commande3Type', $commande);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-reçu');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-qualite:Commandes/valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),

        ));
    }
    public function showqltAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande3Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-reçu');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-qualite:Commandes/show-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * validation commande port logistique
     *
     */
    public function validlogAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande4Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-log-reçu');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-logistique:Commandes/valid-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function showlogAction(Commandes $commande ,Request $request)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\Commande4Type', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comd-log-reçu');
        }
        //dd($editForm);GestionPoleBundle:pole-commande:Commandes/
        return $this->render('GestionPoleBundle:pole-logistique:Commandes/show-commande.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Displays a form to edit an existing commande entity.
     *
     */
    public function editAction(Request $request, Commandes $commande)
    {
        $deleteForm = $this->createDeleteForm($commande);
        $editForm = $this->createForm('GestionPoleBundle\Form\CommandesType', $commande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commandes_edit', array('id' => $commande->getId()));
        }

        return $this->render('commandes/edit.html.twig', array(
            'commande' => $commande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a commande entity.
     *
     */
    public function deleteAction(Request $request, Commandes $commande)
    {
        $form = $this->createDeleteForm($commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commande);
            $em->flush($commande);
        }

        return $this->redirectToRoute('liste-commande');
    }

    /**
     * Creates a form to delete a commande entity.
     *
     * @param Commandes $commande The commande entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Commandes $commande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commandes_delete', array('id' => $commande->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }




    public function refuseAction(Request $request)
    {


    }

    public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-commande:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:pole-commande:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }

    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $msg=$request->get('message');

        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
        $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-reception');

    }
}
