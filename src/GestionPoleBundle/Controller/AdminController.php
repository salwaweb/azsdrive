<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use Symfony\Component\HttpFoundation\Request;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\User;
use GestionPoleBundle\Form\CommandType;
use GestionPoleBundle\Entity\Messagerie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends Controller
{
	 public function statistiqueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('GestionPoleBundle:User', 'u')
            ->addOrderBy('u.id', 'DESC')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%"ROLE_CLIENT"%');
        // ->setParameter('role','%"ROLE_PRODUCTION"%');
        $query = $qb->getQuery();
        $clients = $query->getResult();
        //dd($users);
        //dd($client);
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        //dd($commandes);
        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findAll();
        // dd($commandes);

        return $this->render('GestionPoleBundle:pole-commande:statistique.html.twig', array(

            'commandes'=>$commandes,
            'clients'=>$clients,
            'stockages'=>$stockages
        ));
    }
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function adminAction(Request $request)
    {

        if(true== $this->get('security.authorization_checker')->isGranted('ROLE_RECEPTION'))
        {
            $em = $this->getDoctrine()->getManager();

            $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();

            return $this->render('GestionPoleBundle:pole-commande:index.html.twig', array(
                'commandes' => $commandes,
            ));

        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_PRODUCTION'))

        {
            return $this->render('GestionPoleBundle:Default:production.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_QUALITE'))

        {
            return $this->render('GestionPoleBundle:Default:qualite.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_LOGISTIQUE'))

        {
            return $this->render('GestionPoleBundle:Default:logistique.html.twig');
        }
        elseif(true== $this->get('security.authorization_checker')->isGranted('ROLE_TECHNIQUE'))

        {
            return $this->render('GestionPoleBundle:Default:technique.html.twig');
        }

        return $this->render('GestionPoleBundle:Default:index.html.twig');
    }

    public function adminstrationAction()
    {
        return $this->render('GestionPoleBundle:admin:admin.html.twig', array(
            // ...
        ));
    }
    public function polecmdAction()
    {
        return $this->render('GestionPoleBundle:pole-commande:index.html.twig', array(
            // ...
        ));
    }
    public function messageAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $messagerie->setUtilisateur($utilisateur);
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('recep-messagerie');
        }
        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-commande:messagerie.html.twig', array(
            'messagerie' => $messagerie,
            'messageries' => $messageries,
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));

    }
    public function msgAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        // $idUtilisateur =getUtilisateur($utilisateur)->getId();

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );
        //$commande = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            //array('utilisateur' => $utilisateur, 'refuse' => 1));
        // dd($commandes);
        return $this->render('GestionPoleBundle:pole-commande:liste-message.html.twig', array(
            'messageries' => $messageries,
            //'commande' => $commande,
            'utilisateur' => $utilisateur,
        ));

    }
    public function messagepAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $messagerie->setUtilisateur($utilisateur);
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('prod-messagerie');
        }

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-production:messagerie.html.twig', array(
            'messagerie' => $messagerie,
            'messageries' => $messageries,
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));

    }
    public function messageqAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $messagerie->setUtilisateur($utilisateur);
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('qlt-messages');
        }

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-qualite:messagerie.html.twig', array(
            'messagerie' => $messagerie,
            'messageries' => $messageries,
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));

    }
    public function messagelAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $messagerie->setUtilisateur($utilisateur);
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('log-messagerie');
        }

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-logistique:messagerie.html.twig', array(
            'messagerie' => $messagerie,
            'messageries' => $messageries,
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));

    }
    public function messagetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $messagerie->setUtilisateur($utilisateur);
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('tec-messagerie');
        }

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-technique:messagerie.html.twig', array(
            'messagerie' => $messagerie,
            'messageries' => $messageries,
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));

    }
	 public function gestblAction()
    {
        $em = $this->getDoctrine()->getManager();
        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();
		$bilan= $em->getRepository('GestionPoleBundle:Bilan')->findAll();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        //dd($bilan);

        return $this->render('GestionPoleBundle:admin:GestionCommandes/gestbl.html.twig', array(
            'palettes' => $palettes,
			'bilan' => $bilan,
            'etiquettes' => $etiquettes,
            'commandes'=>$commandes
        ));
    }
    public function poleprodAction()
    {
        return $this->render('GestionPoleBundle:pole-production:index.html.twig', array(
            // ...
        ));
    }
    public function poleqltAction()
    {
        return $this->render('GestionPoleBundle:pole-qualite:index.html.twig', array(
            // ...
        ));
    }
    public function poletecAction()
    {
        return $this->render('GestionPoleBundle:pole-technique:index.html.twig', array(
            // ...
        ));
    }
    public function poleconditionAction()
    {
        return $this->render('GestionPoleBundle:pole-conditionement:index.html.twig', array(
            // ...
        ));
    }
    public function polelogAction()
    {
        return $this->render('GestionPoleBundle:pole-logistique:index.html.twig', array(
            // ...
        ));
    }

    public function ramassageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
       // dd($commandes);
        $ramassages = $em->getRepository('GestionPoleBundle:Palette')->findAll();
        //dd($ramassage);
        return $this->render('GestionPoleBundle:admin:ramassage.html.twig', array(
        'ramassages'=>$ramassages,
            'commandes'=>$commandes
        ));
    }
    public function alerteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        // dd($commandes);

        return $this->render('GestionPoleBundle:admin:alerte.html.twig', array(

            'commandes'=>$commandes
        ));
    }
    public function alertAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        // dd($commandes);

        return $this->render('GestionPoleBundle:admin:alert.html.twig', array(

            'commandes'=>$commandes
        ));
    }


    /**
     * @Route("/commande/", name="admin_page")
     */
    public function administrationAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('GestionPoleBundle:admin:admin.html.twig', []);
    }
    public function listeadmAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => null, 'refuse' => null)
        );

        return $this->render('GestionPoleBundle:admin/GestionCommandes:commande-reçus.html.twig', array(
            'commandes' => $commandes,
        ));

    }



    public function comdadmaccepterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:admin/GestionCommandes:commande-accepter.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function apercu_prodAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-commande:Commandes/apercu-prod.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function apercu_logAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-commande:Commandes/apercu-log.html.twig', array(
            'commandes' => $commandes,
        ));

    }

    public function listeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => null, 'refuse' =>null )
        );

        return $this->render('GestionPoleBundle:pole-commande:Commandes/commande-reçus.html.twig', array(
            'commandes' => $commandes,
        ));

    }

    public function comdaccepterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-commande:Commandes/commande-accepter.html.twig', array(
            'commandes' => $commandes,
        ));

    }



    public function cmdtraitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1)
        );

        return $this->render('GestionPoleBundle:admin:GestionCommandes/cmd-traite.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function cmdprodAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1)
        );

        return $this->render('GestionPoleBundle:admin:GestionCommandes/cmd-prod.html.twig', array(
            'commandes' => $commandes,
        ));

    }

    public function inboxAction(Request $request)
    {
        $numero = mt_rand(000000001, 999999999);

        //dd($numero);
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:admin:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:admin:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }

    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $msg=$request->get('message');

        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
        $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-administration');

    }

    public function stockAction()
    {
        $em = $this->getDoctrine()->getManager();


        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findAll();

        return $this->render('GestionPoleBundle:admin:liststockage.html.twig', array(
            'stockages' => $stockages,
        ));
    }

}
