<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Retouche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Retouche controller.
 *
 */
class RetoucheController extends Controller
{
    /**
     * Lists all retouche entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $retouches = $em->getRepository('GestionPoleBundle:Retouche')->findAll();

        return $this->render('retouche/index.html.twig', array(
            'retouches' => $retouches,
        ));
    }

    /**
     * Creates a new retouche entity.
     *
     */
    public function newAction(Request $request)
    {
        $retouche = new Retouche();
        $form = $this->createForm('GestionPoleBundle\Form\RetoucheType', $retouche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($retouche);
            $em->flush();

            return $this->redirectToRoute('retouche_show', array('id' => $retouche->getId()));
        }

        return $this->render('retouche/new.html.twig', array(
            'retouche' => $retouche,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a retouche entity.
     *
     */
    public function showAction(Retouche $retouche)
    {
        $deleteForm = $this->createDeleteForm($retouche);

        return $this->render('retouche/show.html.twig', array(
            'retouche' => $retouche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing retouche entity.
     *
     */
    public function editAction(Request $request, Retouche $retouche)
    {
        $deleteForm = $this->createDeleteForm($retouche);
        $editForm = $this->createForm('GestionPoleBundle\Form\RetoucheType', $retouche);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('retouche_edit', array('id' => $retouche->getId()));
        }

        return $this->render('retouche/edit.html.twig', array(
            'retouche' => $retouche,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a retouche entity.
     *
     */
    public function deleteAction(Request $request, Retouche $retouche)
    {
        $form = $this->createDeleteForm($retouche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($retouche);
            $em->flush();
        }

        return $this->redirectToRoute('retouche_index');
    }

    /**
     * Creates a form to delete a retouche entity.
     *
     * @param Retouche $retouche The retouche entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Retouche $retouche)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('retouche_delete', array('id' => $retouche->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
