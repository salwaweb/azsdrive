<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Mail;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Mail controller.
 *
 */
class MailController extends Controller
{
    /**
     * Lists all mail entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mails = $em->getRepository('GestionPoleBundle:Mail')->findAll();

        return $this->render('mail/index.html.twig', array(
            'mails' => $mails,
        ));
    }

	 public function contactAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm('LedcastBundle\Form\ContactType', $contact);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $nom=$contact->getNom();
            $prenom=$contact->getPrenom();
            $societe=$contact->getSociete();
            $telephone=$contact->getTelephone();
            $email=$contact->getEmail();
            $sujet=$contact->getSujet();
            $produit=$contact->getProduit();
            $body=$contact->getMessage();
            $mail = \Swift_Message::newInstance()
                ->setSubject('date rendez vous  ')
                ->setFrom('salwa23hleli@gmail.com')
                ->setTo('salwawebdev@gmail.com')

                ->setBody(
                    $this->renderView( 'LedcastBundle:Default:m.html.twig',
                        array('nom'  => $nom,
                            'prenom'  => $prenom,
                            'societe'  => $societe,
                            'email'  => $email,
                            'telephone'  => $telephone,
                            'sujet'  => $sujet,
                            'produit'  => $produit,
                            'body'  => $body,
                            ))
                    ,
                    'text/html'
                );
            $this->get('mailer')->send($mail);
			 if($mail==true){
                echo "Email envoyer avec success ";
            } else{
                echo "Erreur d'envoyer l'email";
            }

            return $this->redirectToRoute('contact_show', array('id' => $contact->getId()));
        }

        return $this->render('LedcastBundle:Default:contact.html.twig', array(
            'contact' => $contact,
            'form' => $form->createView(),
        ));
    }
    /**
     * Creates a new mail entity.
     *
     */
    public function newAction(Request $request)
    {
        $mail = new Mail();
        $form = $this->createForm('GestionPoleBundle\Form\MailType', $mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mail);
            $em->flush();
			$name=$mail->getUsername();
            $email=$mail->getEmail();
            $psw=$mail->getPsw();
			$msg = \Swift_Message::newInstance()
                ->setSubject('compte AZS')
                ->setFrom('azsdrive@gmail.com')
                ->setTo($email)

                ->setBody(
                    $this->renderView( 'GestionPoleBundle:admin:m.html.twig',
                        array('name'  => $name,
                            'email'  => $email,
                            'psw'  => $psw,

                        ))
                    ,
                    'text/html'
                );
            $this->get('mailer')->send($msg);
			 if($msg==true){
                echo "Email envoyer avec success ";
            } else{
                echo "Erreur d'envoyer l'email";
            }


            return $this->redirectToRoute('list_client');
        }

        return $this->render('mail/new.html.twig', array(
            'mail' => $mail,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a mail entity.
     *
     */
    public function showAction(Mail $mail)
    {
        $deleteForm = $this->createDeleteForm($mail);

        return $this->render('mail/show.html.twig', array(
            'mail' => $mail,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing mail entity.
     *
     */
    public function editAction(Request $request, Mail $mail)
    {
        $deleteForm = $this->createDeleteForm($mail);
        $editForm = $this->createForm('GestionPoleBundle\Form\MailType', $mail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mail_edit', array('id' => $mail->getId()));
        }

        return $this->render('mail/edit.html.twig', array(
            'mail' => $mail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a mail entity.
     *
     */
    public function deleteAction(Request $request, Mail $mail)
    {
        $form = $this->createDeleteForm($mail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mail);
            $em->flush();
        }

        return $this->redirectToRoute('mail_index');
    }

    /**
     * Creates a form to delete a mail entity.
     *
     * @param Mail $mail The mail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Mail $mail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mail_delete', array('id' => $mail->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
