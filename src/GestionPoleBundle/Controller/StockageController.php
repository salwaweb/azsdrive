<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Stockage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Stockage controller.
 *
 */
class StockageController extends Controller
{
    /**
     * Lists all stockage entities.
     *
     */
    public function stockAction(Request $request)
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $stockage = new Stockage();
        $form = $this->createForm('GestionPoleBundle\Form\StockageType', $stockage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $stockage->setUtilisateur($utilisateur);
            $stockage->setDate(new \DateTime('now +3 days'));
            $em->persist($stockage);
            $em->flush();

            return $this->redirectToRoute('stockage');
        }

        return $this->render('GestionPoleBundle:client:nvstockage.html.twig', array(
            'utilisateur' => $utilisateur,
            'stockage' => $stockage,
            'form' => $form->createView(),
        ));
    }
    public function stockageAction()
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findBy(
            array('utilisateur' => $utilisateur)
        );

        return $this->render('GestionPoleBundle:client:stockage.html.twig', array(
            'stockages' => $stockages,
        ));
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findAll();

        return $this->render('stockage/index.html.twig', array(
            'stockages' => $stockages,
        ));
    }

    /**
     * Creates a new stockage entity.
     *
     */
    public function newAction(Request $request)
    {
        $stockage = new Stockage();
        $form = $this->createForm('GestionPoleBundle\Form\StockageType', $stockage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($stockage);
            $em->flush();

            return $this->redirectToRoute('stockage_show', array('id' => $stockage->getId()));
        }

        return $this->render('stockage/new.html.twig', array(
            'stockage' => $stockage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a stockage entity.
     *
     */
    public function showAction(Stockage $stockage)
    {
        $deleteForm = $this->createDeleteForm($stockage);

        return $this->render('stockage/show.html.twig', array(
            'stockage' => $stockage,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing stockage entity.
     *
     */
    public function editAction(Request $request, Stockage $stockage)
    {
        $deleteForm = $this->createDeleteForm($stockage);
        $editForm = $this->createForm('GestionPoleBundle\Form\StockageType2', $stockage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('liststock');
        }

        return $this->render('GestionPoleBundle:admin:validstock.html.twig', array(
            'stockage' => $stockage,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a stockage entity.
     *
     */
    public function deleteAction(Request $request, Stockage $stockage)
    {
        $form = $this->createDeleteForm($stockage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($stockage);
            $em->flush();
        }

        return $this->redirectToRoute('stockage_index');
    }

    /**
     * Creates a form to delete a stockage entity.
     *
     * @param Stockage $stockage The stockage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Stockage $stockage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stockage_delete', array('id' => $stockage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
