<?php

namespace GestionPoleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\User;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Form\CommandType;
use Symfony\Component\HttpFoundation\Request;

class QualiteController extends Controller
{
    public function indexAction()
    {
        return $this->render('GestionPoleBundle:pole-qualite:index.html.twig', array(
            // ...
        ));
    }
     public function statistiqueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb->select('u')
            ->from('GestionPoleBundle:User', 'u')
            ->addOrderBy('u.id', 'DESC')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%"ROLE_CLIENT"%');
        // ->setParameter('role','%"ROLE_PRODUCTION"%');
        $query = $qb->getQuery();
        $clients = $query->getResult();
        //dd($users);
        //dd($client);
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findAll();
        //dd($commandes);
        $stockages = $em->getRepository('GestionPoleBundle:Stockage')->findAll();
        // dd($commandes);

        return $this->render('GestionPoleBundle:pole-qualite:statistique.html.twig', array(

            'commandes'=>$commandes,
            'clients'=>$clients,
            'stockages'=>$stockages
        ));
    }
    public function apercu_logAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-qualite:apercu-log.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function recuAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1,'prodfin' => 1 , 'etatqlt' => null)
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:liste-commande.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function encourAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1)
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:commande-encour.html.twig', array(
            'commandes' => $commandes,
        ));


    }
     public function retoucheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1, 'prodfin' => 1 ,'etatqlt' => 0, 'refuse' => 1,'retouche' =>  null)
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:retoucher.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function finretoucheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('prodfin' => 1, 'etatqlt' => 0, 'refuse' => 1,'retouche' => 1)
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:fin-retouche.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function accepterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatqlt' => 1)
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:commande-accepter.html.twig', array(
            'commandes' => $commandes,
        ));

    }

     public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-qualite:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:pole-qualite:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }

    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $msg=$request->get('message');

        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
        $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-qualite');

    }
	  public function certifierAction(Request $request, Commandes $id)
    {
        $cert=$request->get('certfie');

        $em= $this->getDoctrine()->getManager();
        $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $id));

        $comd->setCertifie(true);

        $em->persist($comd); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('comd-accepter');

    }
	public function suiviAction()
    {
        //$cert=$request->get('certfie');
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array()
        );

        return $this->render('GestionPoleBundle:pole-qualite/Commandes:suivi.html.twig', array(
            'commandes' => $commandes,
        ));
    }

}
