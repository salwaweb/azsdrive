<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Palette;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Palette controller.
 *
 */
class PaletteController extends Controller
{
    /**
     * Lists all palette entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $palettes = $em->getRepository('GestionPoleBundle:Palette')->findAll();

        return $this->render('palette/index.html.twig', array(
            'palettes' => $palettes,
        ));
    }

    /**
     * Creates a new palette entity.
     *
     */
    public function newAction(Request $request)
    {
        $palette = new Palette();
        $form = $this->createForm('GestionPoleBundle\Form\PaletteType', $palette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($palette);
            $em->flush();

            return $this->redirectToRoute('palette_show', array('id' => $palette->getId()));
        }

        return $this->render('palette/new.html.twig', array(
            'palette' => $palette,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a palette entity.
     *
     */
    public function showAction(Palette $palette)
    {
        $deleteForm = $this->createDeleteForm($palette);

        return $this->render('palette/show.html.twig', array(
            'palette' => $palette,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing palette entity.
     *
     */
    public function editAction(Request $request, Palette $palette)
    {
        $deleteForm = $this->createDeleteForm($palette);
        $editForm = $this->createForm('GestionPoleBundle\Form\PaletteType', $palette);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('palette_edit', array('id' => $palette->getId()));
        }

        return $this->render('palette/edit.html.twig', array(
            'palette' => $palette,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a palette entity.
     *
     */
    public function deleteAction(Request $request, Palette $palette)
    {
        $form = $this->createDeleteForm($palette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($palette);
            $em->flush();
        }

        return $this->redirectToRoute('palette_index');
    }

    /**
     * Creates a form to delete a palette entity.
     *
     * @param Palette $palette The palette entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Palette $palette)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('palette_delete', array('id' => $palette->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
