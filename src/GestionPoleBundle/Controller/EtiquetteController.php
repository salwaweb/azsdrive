<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Etiquette;
use GestionPoleBundle\Entity\Commandes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Etiquette controller.
 *
 */
class EtiquetteController extends Controller
{
    /**
     * Lists all etiquette entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etiquettes = $em->getRepository('GestionPoleBundle:Etiquette')->findAll();

        return $this->render('etiquette/index.html.twig', array(
            'etiquettes' => $etiquettes,
        ));
    }

    /**
     * Creates a new etiquette entity.
     *
     */
    public function newAction(Commandes $commande,Request $request)
    {
        $etiquette = new Etiquette();
        //$commande ->setVoyages($commande);
        $form = $this->createForm('GestionPoleBundle\Form\EtiquetteType', $etiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etiquette);
            $em->flush();

            return $this->redirectToRoute('etiquette_show', array('id' => $etiquette->getId()));
        }

        return $this->render('etiquette/new.html.twig', array(
            //'commande'=> $commande,
            'etiquette' => $etiquette,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a etiquette entity.
     *
     */
    public function showAction(Etiquette $etiquette)
    {
        $deleteForm = $this->createDeleteForm($etiquette);

        return $this->render('etiquette/show.html.twig', array(
            'etiquette' => $etiquette,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing etiquette entity.
     *
     */
    public function editAction(Request $request, Etiquette $etiquette)
    {
        $deleteForm = $this->createDeleteForm($etiquette);
        $editForm = $this->createForm('GestionPoleBundle\Form\EtiquetteType', $etiquette);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('etiquette_edit', array('id' => $etiquette->getId()));
        }

        return $this->render('etiquette/edit.html.twig', array(
            'etiquette' => $etiquette,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a etiquette entity.
     *
     */
    public function deleteAction(Request $request, Etiquette $etiquette)
    {
        $form = $this->createDeleteForm($etiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etiquette);
            $em->flush();
        }

        return $this->redirectToRoute('etiquette_index');
    }

    /**
     * Creates a form to delete a etiquette entity.
     *
     * @param Etiquette $etiquette The etiquette entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Etiquette $etiquette)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etiquette_delete', array('id' => $etiquette->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
