<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Prod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Prod controller.
 *
 */
class ProdController extends Controller
{
    /**
     * Lists all prod entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $prods = $em->getRepository('GestionPoleBundle:Prod')->findAll();

        return $this->render('prod/index.html.twig', array(
            'prods' => $prods,
        ));
    }

    /**
     * Creates a new prod entity.
     *
     */
    public function newAction(Request $request)
    {
        $prod = new Prod();
        $form = $this->createForm('GestionPoleBundle\Form\ProdType', $prod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prod);
            $em->flush($prod);

            return $this->redirectToRoute('prod_show', array('id' => $prod->getId()));
        }

        return $this->render('prod/new.html.twig', array(
            'prod' => $prod,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a prod entity.
     *
     */
    public function showAction(Prod $prod)
    {
        $deleteForm = $this->createDeleteForm($prod);

        return $this->render('prod/show.html.twig', array(
            'prod' => $prod,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing prod entity.
     *
     */
    public function editAction(Request $request, Prod $prod)
    {
        $deleteForm = $this->createDeleteForm($prod);
        $editForm = $this->createForm('GestionPoleBundle\Form\ProdType', $prod);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('prod_edit', array('id' => $prod->getId()));
        }

        return $this->render('prod/edit.html.twig', array(
            'prod' => $prod,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a prod entity.
     *
     */
    public function deleteAction(Request $request, Prod $prod)
    {
        $form = $this->createDeleteForm($prod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($prod);
            $em->flush($prod);
        }

        return $this->redirectToRoute('prod_index');
    }

    /**
     * Creates a form to delete a prod entity.
     *
     * @param Prod $prod The prod entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Prod $prod)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prod_delete', array('id' => $prod->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
