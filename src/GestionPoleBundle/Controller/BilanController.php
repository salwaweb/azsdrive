<?php

namespace GestionPoleBundle\Controller;

//use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\Bilan;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bilan controller.
 *
 */
class BilanController extends Controller
{
    /**
     * Lists all bilan entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $bilans = $em->getRepository('GestionPoleBundle:Bilan')->findAll();

        return $this->render('bilan/index.html.twig', array(
            'bilans' => $bilans,
        ));
    }

    /**
     * Creates a new bilan entity.
     *
     */
    public function newAction(Request $request)
    {

        $bilan = new Bilan();

        $form = $this->createForm('GestionPoleBundle\Form\BilanType', $bilan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bilan);
            $em->flush();

            return $this->redirectToRoute('bilan_show', array('id' => $bilan->getId()));
        }

        return $this->render('bilan/new.html.twig', array(
            'bilan' => $bilan,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a bilan entity.
     *
     */
    public function showAction(Bilan $bilan)
    {
        $deleteForm = $this->createDeleteForm($bilan);

        return $this->render('bilan/show.html.twig', array(
            'bilan' => $bilan,
            'delete_form' => $deleteForm->createView(),
        ));
    }

	   public function editblAction(bilan $bilan, Request $request)
    {
      
       //dd($nbrplt);
	   
	   $em= $this->getDoctrine()->getManager();
		$piece=$request->get('nompiece');
		$qent=$request->get('qtentre');
		$qsrt=$request->get('qtsort');
		$complet=$request->get('crtcomplet');
		$incomplet=$request->get('crtincomplet');
		$qtsrt=$request->get('qtsort');
		$nbp=$request->get('nbrplt');
		$nomp=$request->get('nomplt');
		$pdec=$request->get('prixdec');
		
       $bl = $em->getRepository('GestionPoleBundle:Bilan')->findOneBy(array('id' => $bilan));
	  
        //$bilan ->setUtilisateur($cl);
        $bl ->setNompiece($piece);
		$bl ->setQtentre($qent);
        $bl ->setQtsort($qsrt);
        $bl->setNbrpalet($nbp);
        $bl ->setNompalet($nomp);
        $bl ->setCrtcomplet($complet);
        $bl ->setCrtincomplet($incomplet);
        $bl ->setPrixdec($pdec);
		$em = $this->getDoctrine()->getManager();
        $em->persist($bl);
        $em->flush();
//dd($access);
		
		
		
		
         return $this->redirectToRoute('gestionbl');

     
    }
    /**
     * Displays a form to edit an existing bilan entity.
     *
     */
    public function editAction(Request $request, Bilan $bilan)
    {
        $deleteForm = $this->createDeleteForm($bilan);
        $editForm = $this->createForm('GestionPoleBundle\Form\BilanType', $bilan);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bilan_edit', array('id' => $bilan->getId()));
        }

        return $this->render('bilan/edit.html.twig', array(
            'bilan' => $bilan,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a bilan entity.
     *
     */
    public function deleteAction(Request $request, Bilan $bilan)
    {
        $form = $this->createDeleteForm($bilan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bilan);
            $em->flush();
        }

        return $this->redirectToRoute('bilan_index');
    }

    /**
     * Creates a form to delete a bilan entity.
     *
     * @param Bilan $bilan The bilan entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bilan $bilan)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bilan_delete', array('id' => $bilan->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
