<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Fabrication;
use GestionPoleBundle\Entity\Commandes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Spipu\Html2Pdf\Html2Pdf;

/**
 * Fabrication controller.
 *
 */
class FabricationController extends Controller
{
    /**
     * Lists all fabrication entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fabrications = $em->getRepository('GestionPoleBundle:Fabrication')->findAll();

        return $this->render('fabrication/index.html.twig', array(
            'fabrications' => $fabrications,
        ));
    }

    /**
     * Creates a new fabrication entity.
     *
     */
    public function newAction(Request $request)
    {
        $fabrication = new Fabrication();
        $form = $this->createForm('GestionPoleBundle\Form\FabricationType', $fabrication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fabrication);
            $em->flush();

            return $this->redirectToRoute('fabrication_show', array('id' => $fabrication->getId()));
        }

        return $this->render('fabrication/new.html.twig', array(
            'fabrication' => $fabrication,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fabrication entity.
     *
     */
    public function showAction(Fabrication $fabrication)
    {
        $deleteForm = $this->createDeleteForm($fabrication);

        return $this->render('fabrication/show.html.twig', array(
            'fabrication' => $fabrication,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fabrication entity.
     *
     */
    public function editAction(Request $request, Fabrication $fabrication)
    {
        $deleteForm = $this->createDeleteForm($fabrication);
        $editForm = $this->createForm('GestionPoleBundle\Form\FabricationType', $fabrication);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fabrication_edit', array('id' => $fabrication->getId()));
        }

        return $this->render('fabrication/edit.html.twig', array(
            'fabrication' => $fabrication,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fabrication entity.
     *
     */
    public function deleteAction(Request $request, Fabrication $fabrication)
    {
        $form = $this->createDeleteForm($fabrication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fabrication);
            $em->flush();
        }

        return $this->redirectToRoute('fabrication_index');
    }

    /**
     * Creates a form to delete a fabrication entity.
     *
     * @param Fabrication $fabrication The fabrication entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fabrication $fabrication)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fabrication_delete', array('id' => $fabrication->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function fabAction(Commandes $commandes,Request $request)
    {
       //var_dump($request->get('nbrpiece'));die();
        $nomdec=$request->get('nomdec');
        $jour=$request->get('jour');
        $type=$request->get('type');
        $nbrpasse=$request->get('nbrpasse');
        $quantite=$request->get('quantite');
$em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commandes));
		$comd->setPlt(true);
        $fabrication = new Fabrication();

        $fabrication ->setCommande($commandes);
        $fabrication->setNomdec($nomdec);
        $fabrication->setJour($jour);
        $fabrication->setType($type);
        $fabrication->setNbrpasse($nbrpasse);
        $fabrication->setQuantite($quantite);
        $em = $this->getDoctrine()->getManager();
        $em->persist($fabrication, $comd);
        $em->flush();

        return $this->redirectToRoute('gestion-fabrication');

    }
    public function fabricationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $fabrications = $em->getRepository('GestionPoleBundle:Fabrication')->findAll();
        //dd($etiquettes);
        // dd($form);
        return $this->render('GestionPoleBundle:pole-commande/Commandes:liste-fabrication.html.twig', array(
            'fabrications' => $fabrications,

        ));
    }

    public function fabpdfAction(Fabrication $fabrication)
    {
        $html = $this->renderView('GestionPoleBundle:pole-commande:Commandes/fab-pdf.html.twig', array('fabrication' => $fabrication));

        $html2pdf = new Html2Pdf('P','A6','fr');
        $html2pdf->pdf->SetAuthor('O.fabrication');
        $html2pdf->pdf->SetTitle('O.fabrication ');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('O.fabrication.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
}
