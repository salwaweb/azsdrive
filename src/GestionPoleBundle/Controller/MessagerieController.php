<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Messagerie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Messagerie controller.
 *
 */
class MessagerieController extends Controller
{
    /**
     * Lists all messagerie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $messageries = $em->getRepository('GestionPoleBundle:Messagerie')->findAll();

        return $this->render('messagerie/index.html.twig', array(
            'messageries' => $messageries,
        ));
    }

    /**
     * Creates a new messagerie entity.
     *
     */
    public function newAction(Request $request)
    {
        $messagerie = new Messagerie();
        $form = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($messagerie);
            $em->flush();

            return $this->redirectToRoute('messagerie_show', array('id' => $messagerie->getId()));
        }

        return $this->render('messagerie/new.html.twig', array(
            'messagerie' => $messagerie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a messagerie entity.
     *
     */
    public function showAction(Messagerie $messagerie)
    {
        $deleteForm = $this->createDeleteForm($messagerie);

        return $this->render('messagerie/show.html.twig', array(
            'messagerie' => $messagerie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing messagerie entity.
     *
     */
    public function editAction(Request $request, Messagerie $messagerie)
    {
        $deleteForm = $this->createDeleteForm($messagerie);
        $editForm = $this->createForm('GestionPoleBundle\Form\MessagerieType', $messagerie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('messagerie_edit', array('id' => $messagerie->getId()));
        }

        return $this->render('messagerie/edit.html.twig', array(
            'messagerie' => $messagerie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a messagerie entity.
     *
     */
    public function deleteAction(Request $request, Messagerie $messagerie)
    {
        $form = $this->createDeleteForm($messagerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($messagerie);
            $em->flush();
        }

        return $this->redirectToRoute('messagerie_index');
    }

    /**
     * Creates a form to delete a messagerie entity.
     *
     * @param Messagerie $messagerie The messagerie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Messagerie $messagerie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('messagerie_delete', array('id' => $messagerie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
