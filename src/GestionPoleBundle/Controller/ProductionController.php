<?php

namespace GestionPoleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\User;
use GestionPoleBundle\Entity\Sav;
use GestionPoleBundle\Entity\Tiket;
use GestionPoleBundle\Form\CommandType;
use Symfony\Component\HttpFoundation\Request;

class ProductionController extends Controller
{
    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAction()
    {
        return $this->render('GestionPoleBundle:pole-production:index.html.twig', array(
            // ...
        ));
    }

    public function ordrefabAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1,'ecran' => 1, 'etatprod' => null)
        );

        return $this->render('GestionPoleBundle:pole-production/Commandes:ordre-fab.html.twig', array(
            'commandes' => $commandes,
        ));

    }
     public function encourAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1,'prodfin' => null)
        );
//dd($commandes);
        return $this->render('GestionPoleBundle:pole-production/Commandes:commande-encour.html.twig', array(
            'commandes' => $commandes,
        ));


    }
    public function termineAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('prodfin' =>1 )
        );

        return $this->render('GestionPoleBundle:pole-production/Commandes:commande-termine.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function retoucheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array( 'prodfin' => 1 , 'etatqlt' => 0,'refuse'=> 1, 'retouche' => null)
        );

        return $this->render('GestionPoleBundle:pole-production/Commandes:retouche.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function finretoucheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array( 'prodfin' => 1 , 'etatqlt' => 0,'refuse'=> 1, 'retouche' => 1)
        );

        return $this->render('GestionPoleBundle:pole-production/Commandes:retouche-termines.html.twig', array(
            'commandes' => $commandes,
        ));

    }
   
    public function conditionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatcomd' => 1)
        );

        return $this->render('GestionPoleBundle:pole-production/Commandes:conditionnement.html.twig', array(
            'commandes' => $commandes,
        ));

    }

    public function apercu_logAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array('etatprod' => 1)
        );

        return $this->render('GestionPoleBundle:pole-production:Commandes/apercu-log.html.twig', array(
            'commandes' => $commandes,
        ));

    }
    public function apercu_recpAction()
    {
        $em = $this->getDoctrine()->getManager();
        $commandes = $em->getRepository('GestionPoleBundle:Commandes')->findBy(
            array(
            ));

        return $this->render('GestionPoleBundle:pole-production:Commandes/apercu-reception.html.twig', array(
            'commandes' => $commandes,
        ));

    }

     public function inboxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findBy(
            array('pole' => $utilisateur)
        );


        return $this->render('GestionPoleBundle:pole-production:inbox.html.twig', array(
            'savs' => $savs,

        ));


    }
    public function showticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $savs = $em->getRepository('GestionPoleBundle:Sav')->findAll();
        $em = $this->getDoctrine()->getManager();
        $tikets = $em->getRepository('GestionPoleBundle:Tiket')->findBy(
            array('support' => $sav));

        return $this->render('GestionPoleBundle:pole-production:show-ticket.html.twig', array(
            'utilisateur' => $utilisateur,
            'savs' => $savs,
            'tikets' => $tikets,
            'sav' => $sav,

        ));
    }

    public function addticketAction(Sav $sav, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $msg=$request->get('message');

        $tiket = new Tiket();
        $tiket ->setSupport($sav);
        $tiket ->setSender($utilisateur);
        $tiket ->getSupport();

        $tiket ->setMessage($msg);
        //dd($tiket);
        $em = $this->getDoctrine()->getManager();
        $em->persist($tiket);
        $em->flush();

        return $this->redirectToRoute('inbox-production');

    }
}
