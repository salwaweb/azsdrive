<?php

namespace GestionPoleBundle\Controller;

use GestionPoleBundle\Entity\Retiquette;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use GestionPoleBundle\Entity\Commandes;
use GestionPoleBundle\Entity\Etiquette;
use Spipu\Html2Pdf\Html2Pdf;

/**
 * Retiquette controller.
 *
 */
class RetiquetteController extends Controller
{
    /**
     * Lists all retiquette entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $retiquettes = $em->getRepository('GestionPoleBundle:Retiquette')->findAll();

        return $this->render('retiquette/index.html.twig', array(
            'retiquettes' => $retiquettes,
        ));
    }

    /**
     * Creates a new retiquette entity.
     *
     */
    public function newAction(Request $request)
    {
        $retiquette = new Retiquette();
        $form = $this->createForm('GestionPoleBundle\Form\RetiquetteType', $retiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($retiquette);
            $em->flush();

            return $this->redirectToRoute('retiquette_show', array('id' => $retiquette->getId()));
        }

        return $this->render('retiquette/new.html.twig', array(
            'retiquette' => $retiquette,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a retiquette entity.
     *
     */
    public function showAction(Retiquette $retiquette)
    {
        $deleteForm = $this->createDeleteForm($retiquette);

        return $this->render('retiquette/show.html.twig', array(
            'retiquette' => $retiquette,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing retiquette entity.
     *
     */
    public function editAction(Request $request, Retiquette $retiquette)
    {
        $deleteForm = $this->createDeleteForm($retiquette);
        $editForm = $this->createForm('GestionPoleBundle\Form\RetiquetteType', $retiquette);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('retiquette_edit', array('id' => $retiquette->getId()));
        }

        return $this->render('retiquette/edit.html.twig', array(
            'retiquette' => $retiquette,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a retiquette entity.
     *
     */
    public function deleteAction(Request $request, Retiquette $retiquette)
    {
        $form = $this->createDeleteForm($retiquette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($retiquette);
            $em->flush();
        }

        return $this->redirectToRoute('retiquette_index');
    }

    /**
     * Creates a form to delete a retiquette entity.
     *
     * @param Retiquette $retiquette The retiquette entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Retiquette $retiquette)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('retiquette_delete', array('id' => $retiquette->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function retiquetteAction(Commandes $commandes,Request $request)
    {

        //var_dump($request->get('nbrpiece'));die();
        $total=$request->get('total');
        $adresse=$request->get('adresse');
        $tel=$request->get('tel');
        $numero = mt_rand(0001, 9999);
	$em= $this->getDoctrine()->getManager();

       $comd = $em->getRepository('GestionPoleBundle:Commandes')->findOneBy(array('id' => $commandes));
		$comd->setRetq(true);

        $retiquette = new Retiquette();
        $retiquette ->setCommande($commandes);
        $retiquette->setTotal($total);
        $retiquette->setNumero($numero);
        $retiquette->setTel($tel);
        $retiquette->setAdresse($adresse);
        $em = $this->getDoctrine()->getManager();
        $em->persist($retiquette, $comd);
        $em->flush();

        return $this->redirectToRoute('gestion-palette');

    }
    public function gestionretiquetteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $retiquettes = $em->getRepository('GestionPoleBundle:Retiquette')->findAll();
        //dd($etiquettes);
        // dd($form);
        return $this->render('GestionPoleBundle:pole-commande/Commandes:liste-palette.html.twig', array(
            'retiquettes' => $retiquettes,

        ));
    }
    public function etqtpdfAction(Retiquette $retiquette)
    {
        $html = $this->renderView('GestionPoleBundle:pole-commande:Commandes/etqt-pdf.html.twig', array('retiquette' => $retiquette));

        $html2pdf = new Html2Pdf('P','A6','fr');
        $html2pdf->pdf->SetAuthor('Palette');
        $html2pdf->pdf->SetTitle('Palette ');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('F.palette.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }
    public function palettepdfAction(Retiquette $retiquette)
    {
        $html = $this->renderView('GestionPoleBundle:pole-commande:Commandes/palette-pdf.html.twig', array('retiquette' => $retiquette));

        $html2pdf = new Html2Pdf('P','A6','fr');
        
		$html2pdf->pdf->SetAuthor('Etiquette');
        $html2pdf->pdf->SetTitle('Etiquette');
        $html2pdf->pdf->SetSubject('Edition Final du BL');
        $html2pdf->pdf->SetKeywords('Edition Final du BL');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);
        $html2pdf->Output('Etiquette.pdf');

        $response = new Response();
        $response->headers->set('Content-type' , 'application/pdf');

        return $response;
    }

}
